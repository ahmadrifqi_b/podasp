<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Rubah Data Barang') }}
        </h2>
    </x-slot>

    <div class="py-10">
        <div class="w-3/4 mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg border-lg border">
                <div class="w-full lg:w-3/4 m-5 pr-0 lg:pr-2">
                    <div class="leading-loose">
                        <form action="/formulir/{{ $data->id }}/item/update/{{ $data->id }}" method="POST" class="p-0 lg:py-5 lg:px-10">
                            @method('PUT')
                            @csrf
                            <div class="mt-4">
                                <label class="block text-base text-gray-600 font-semibold" for="item_name">Nama Barang</label>
                                <input class="w-full px-5 py-3 transition duration-300 border border-gray-300 rounded focus:border-transparent focus:outline-none focus:ring-4 focus:ring-blue-200" id="item_name" name="item_name" type="text" required placeholder="Nama Barang" value="{{ $data->item_name }}">
                            </div>
                            <div class="mt-4">
                                <label class="block text-base text-gray-600 font-semibold" for="type_data_id">Jenis Barang</label>
                                <select class="w-full px-5 py-3 transition duration-300 border border-gray-300 rounded focus:border-transparent focus:outline-none focus:ring-4 focus:ring-blue-200" id="type_data_id" name="type_data_id" placeholder="Jenis Barang">
                                    <option value="{{ $data->category->id }}">{{ $data->category->type_name }}</option>
                                    @foreach ($category as $item)
                                        <option value="{{ $item->id }}">{{ $item->type_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mt-4">
                                <label class="block text-base text-gray-600 font-semibold" for="unit">Satuan</label>
                                <input class="w-full px-5 py-3 transition duration-300 border border-gray-300 rounded focus:border-transparent focus:outline-none focus:ring-4 focus:ring-blue-200" id="unit" name="unit" placeholder="Satuan" value="{{ $data->unit }}">
                            </div>
                            <div class="mt-4">
                                <label class="block text-base text-gray-600 font-semibold" for="quantity">Banyaknya Barang</label>
                                <input class="w-full px-5 py-3 transition duration-300 border border-gray-300 rounded focus:border-transparent focus:outline-none focus:ring-4 focus:ring-blue-200" id="quantity" name="quantity" type="text" placeholder="Banyaknya Barang" value="{{ $data->quantity }}">
                            </div>
                            <div class="mt-4">
                                <label class="block text-base text-gray-600 font-semibold" for="price">Harga Satuan</label>
                                <input class="w-full px-5 py-3 transition duration-300 border border-gray-300 rounded focus:border-transparent focus:outline-none focus:ring-4 focus:ring-blue-200" id="price" name="price" type="text" placeholder="Harga" value="{{ $data->price }}">
                            </div>
                            <div class="mt-4">
                                <label class="block text-base text-gray-600 font-semibold" for="supplier_id">Pemasok Barang</label>
                                <select class="w-full px-5 py-3 transition duration-300 border border-gray-300 rounded focus:border-transparent focus:outline-none focus:ring-4 focus:ring-blue-200" id="supplier_id" name="supplier_id" placeholder="Supplier">
                                    <option value="{{ $data->supplier->id }}">{{ $data->supplier->supplier_name }}</option>
                                    @foreach ($supplier as $item)
                                        <option value="{{ $item->id }}">{{ $item->supplier_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="inline-block mr-2 mt-6">
                                <button type="submit" class="focus:outline-none font-light tracking-wider text-white py-2 px-5 rounded-md bg-blue-500 hover:bg-blue-600 hover:shadow-lg">Simpan</button>
                             </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
