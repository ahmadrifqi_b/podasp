<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Rubah Data Formulir Purchase Order') }}
        </h2>
    </x-slot>

    <div class="py-10">
        <div class="w-3/4 mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg border-lg border">
                <div class="w-full lg:w-3/4 m-5 pr-0 lg:pr-2">
                    <div class="leading-loose">
                        <form action="{{ route('formulir.update', $data->id) }}" method="POST" class="p-0 lg:py-5 lg:px-10">
                            @method('PUT')
                            @csrf
                            <div class="mt-0">
                                <label class="block text-base text-gray-600 font-semibold" for="po_number">Number</label>
                                <input class="w-full px-5 py-3 transition duration-300 border border-gray-300 rounded focus:border-transparent focus:outline-none focus:ring-4 focus:ring-blue-200" id="po_number" name="po_number" type="text" placeholder="Nomor Formulir" value="{{ $data->po_number }}">
                            </div>
                            <div class="mt-4">
                                <label class="block text-base text-gray-600 font-semibold" for="project_name">Nama</label>
                                <input class="w-full px-5 py-3 transition duration-300 border border-gray-300 rounded focus:border-transparent focus:outline-none focus:ring-4 focus:ring-blue-200" id="project_name" name="project_name" type="text" required placeholder="Nama Formulir" value="{{ $data->project_name }}">
                            </div>
                            <div class="mt-4">
                                <label class="block text-base text-gray-600 font-semibold" for="address">Alamat</label>
                                <input class="w-full px-5 py-3 transition duration-300 border border-gray-300 rounded focus:border-transparent focus:outline-none focus:ring-4 focus:ring-blue-200" id="address" name="address" type="text" placeholder="Alamat Proyek" value="{{ $data->address }}">
                            </div>
                            <div class="mt-4">
                                <label class="block text-base text-gray-600 font-semibold" for="date">Tanggal</label>
                                <input class="w-full px-5 py-3 transition duration-300 border border-gray-300 rounded focus:border-transparent focus:outline-none focus:ring-4 focus:ring-blue-200" id="date" name="date" type="date" required placeholder="Tanggal Pembuatan" value="{{ $data->date }}">
                            </div>
                            <div class="mt-4">
                                <label class="block text-base text-gray-600 font-semibold" for="delivery">Pengiriman</label>
                                <input class="w-full px-5 py-3 transition duration-300 border border-gray-300 rounded focus:border-transparent focus:outline-none focus:ring-4 focus:ring-blue-200" id="delivery" name="delivery" type="text" placeholder="Tujuan Pengiriman" value="{{ $data->delivery }}">
                            </div>
                            <div class="mt-4">
                                <label class="block text-base text-gray-600 font-semibold" for="telephone">Telepon</label>
                                <input class="w-full px-5 py-3 transition duration-300 border border-gray-300 rounded focus:border-transparent focus:outline-none focus:ring-4 focus:ring-blue-200" id="telephone" name="telephone" type="text" placeholder="Nomor Telepon" value="{{ $data->telephone }}">
                            </div>
                            <div class="mt-4">
                                <label class="block text-base text-gray-600 font-semibold" for="email">Email</label>
                                <input class="w-full px-5 py-3 transition duration-300 border border-gray-300 rounded focus:border-transparent focus:outline-none focus:ring-4 focus:ring-blue-200" id="email" name="email" type="email" placeholder="Email" value="{{ $data->email }}">
                            </div>
                            <div class="mt-4">
                                <label class="block text-base text-gray-600 font-semibold" for="fax">FAX</label>
                                <input class="w-full px-5 py-3 transition duration-300 border border-gray-300 rounded focus:border-transparent focus:outline-none focus:ring-4 focus:ring-blue-200" id="fax" name="fax" type="text" placeholder="FAX" value="{{ $data->fax }}">
                            </div>
                            <div class="inline-block mr-2 mt-6">
                                <button type="submit" class="focus:outline-none font-light tracking-wider text-white py-2 px-5 rounded-md bg-blue-500 hover:bg-blue-600 hover:shadow-lg">Simpan</button>
                             </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
