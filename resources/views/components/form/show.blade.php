<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Formulir Purchase Order') }}
        </h2>
    </x-slot>

    <div class="py-10">
        <div class="w-3/4 mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg border-lg border">
                <div class="w-full m-5 pr-0 lg:pr-2">
                    <div class="flex mb-8 justify-between leading-loose">
                        <div class="w-7/12">
                            <div class="m-5 md:mb-1 md:flex items-center">
                                <label class="w-32 text-gray-800 block font-bold text-sm uppercase tracking-wide">PO Number</label>
                                <span class="mr-4 inline-block hidden md:block">:</span>
                                <div class="flex-1">{{ $data->po_number }}</div>
                            </div>
                            <div class="m-5 md:mb-1 md:flex items-center">
                                <label class="w-32 text-gray-800 block font-bold text-sm uppercase tracking-wide">Alamat</label>
                                <span class="mr-4 inline-block hidden md:block">:</span>
                                <div class="flex-1">{{ $data->address }}</div>
                            </div>
                            <div class="m-5 md:mb-1 md:flex items-center">
                                <label class="w-32 text-gray-800 block font-bold text-sm uppercase tracking-wide">Telepon</label>
                                <span class="mr-4 inline-block hidden md:block">:</span>
                                <div class="flex-1">{{ $data->telephone }}</div>
                            </div>
                            <div class="m-5 md:mb-1 md:flex items-center">
                                <label class="w-32 text-gray-800 block font-bold text-sm uppercase tracking-wide">Email</label>
                                <span class="mr-4 inline-block hidden md:block">:</span>
                                <div class="flex-1">{{ $data->email }}</div>
                            </div>
                            <div class="m-5 md:mb-1 md:flex items-center">
                                <label class="w-32 text-gray-800 block font-bold text-sm uppercase tracking-wide">FAX</label>
                                <span class="mr-4 inline-block hidden md:block">:</span>
                                <div class="flex-1">{{ $data->fax }}</div>
                            </div>
                        </div>
                        <div class="w-5/12">
                            <div class="m-5 md:mb-1 md:flex items-center">
                                <label class="w-32 text-gray-800 block font-bold text-sm uppercase tracking-wide">Tanggal</label>
                                <span class="mr-4 inline-block hidden md:block">:</span>
                                <div class="flex-1">{{ $data->date }}</div>
                            </div>
                            <div class="m-5 md:mb-1 md:flex items-center">
                                <label class="w-32 text-gray-800 block font-bold text-sm uppercase tracking-wide">Nama</label>
                                <span class="mr-4 inline-block hidden md:block">:</span>
                                <div class="flex-1">{{ $data->project_name }}</div>
                            </div>
                            <div class="m-5 md:mb-1 md:flex items-center">
                                <label class="w-32 text-gray-800 block font-bold text-sm uppercase tracking-wide">Pengiriman</label>
                                <span class="mr-4 inline-block hidden md:block">:</span>
                                <div class="flex-1">{{ $data->delivery }}</div>
                            </div>
                            <div class="m-5 md:mb-1 md:flex items-center">
                                <label class="w-32 text-gray-800 block font-bold text-sm uppercase tracking-wide">Purchasing Contact</label>
                                <span class="mr-4 inline-block hidden md:block">:</span>
                                <div class="flex-1">{{ $data->user_id }}</div>
                            </div>
                            @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
                            <div class="m-5 md:mb-1 md:flex items-center">
                                <div class="inline-block mr-2 mt-2">
                                    {{-- {{ route('formulir', $data->id, 'item.create') }} --}}
                                    <a href="/formulir/{{ $data->id }}/item/create" class="px-4 py-2 text-white font-light tracking-wider focus:outline-none rounded-md bg-blue-500 hover:bg-blue-600 hover:shadow-lg">Tambah Item</a>
                                </div>
                                <div class="inline-block mr-2 mt-2">
                                    <a href="{{ route('formulir.edit', $data->id) }}" class="px-4 py-2 text-white font-light tracking-wider focus:outline-none rounded-md bg-yellow-500 hover:bg-yellow-600 hover:shadow-lg">Rubah Formulir</a>
                                </div>
                            </div>
                            @endif
                            @if (Auth::user()->role_id != 3)
                            <div class="m-5 md:mb-1 md:flex items-center">
                                @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
                                <div class="inline-block mr-2 mt-2">
                                    {{-- {{ route('formulir', $data->id, 'item.create') }} --}}
                                    <a href="{{ route('formulir.approval', $data->id) }}" class="px-4 py-2 text-white font-light tracking-wider focus:outline-none rounded-md bg-blue-500 hover:bg-blue-600 hover:shadow-lg">Persetujuan</a>
                                </div>
                                @endif
                                @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 4)
                                <div class="inline-block mr-2 mt-2">
                                    <a href="{{ route('formulir.funds', $data->id) }}" class="px-4 py-2 text-white font-light tracking-wider focus:outline-none rounded-md bg-blue-500 hover:bg-blue-600 hover:shadow-lg">Pencairan</a>
                                </div>
                                @endif
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="mr-10 m-5 p-5 border-4 rounded">
                        <table class="text-left w-full">
                            <thead>
                                <tr>
                                    <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">No</th>
                                    <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Nama Item</th>
                                    <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Jenis</th>
                                    <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Banyak</th>
                                    <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Harga</th>
                                    <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Jumlah</th>
                                    @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
                                    <th class="py-2 px-2 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Aksi</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1?>
                                @foreach ($data->form as $formData)
                                <?php $total = $formData->quantity * $formData->price ?>
                                <tr class="hover:bg-grey-lighter">
                                    <td class="py-2 px-2 border-b border-grey-light">{{ $no++ }}</td>
                                    <td class="py-2 px-2 border-b border-grey-light">{{ $formData->item_name }}</td>
                                    <td class="py-2 px-2 border-b border-grey-light">{{ $formData->type_data_id }}</td>
                                    <td class="py-2 px-2 border-b border-grey-light">{{ $formData->quantity }}</td>
                                    <td class="py-2 px-2 border-b border-grey-light">{{ $formData->price }}</td>
                                    <td class="py-2 px-2 border-b border-grey-light">{{ $total }}</td>
                                    @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
                                    <td class="py-2 px-2 border-b border-grey-light">
                                        {{-- <div class="inline-block mr-2 mt-2">
                                            <a href="/formulir/{{ $data->id }}/item/show/{{ $formData->id }}" class="focus:outline-none text-white py-2 px-2 rounded-md bg-yellow-500 hover:bg-yellow-600 hover:shadow-lg">Ubah</a>
                                        </div> --}}
                                        <div class="inline-block mr-2 mt-2">
                                            <a href="/formulir/{{ $data->id }}/item/delete/{{ $formData->id }}" class="focus:outline-none text-white py-2 px-2 rounded-md bg-red-500 hover:bg-red-600 hover:shadow-lg">Hapus</a>
                                        </div>
                                    </td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
