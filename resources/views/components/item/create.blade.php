<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Tambah Data Kategori Item Barang') }}
        </h2>
    </x-slot>

    <div class="py-10">
        <div class="w-3/4 mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg border-lg border">
                <div class="w-full lg:w-3/4 m-5 pr-0 lg:pr-2">
                    <div class="leading-loose">
                        <form action="{{ route('kategori.store') }}" method="POST" class="p-0 lg:py-5 lg:px-10">
                            @csrf
                            <div class="mt-0">
                                <label class="block text-base text-gray-600 font-semibold" for="type_name">Nama Kategori</label>
                                <input class="w-full px-5 py-3 transition duration-300 border border-gray-300 rounded focus:border-transparent focus:outline-none focus:ring-4 focus:ring-blue-200" id="type_name" name="type_name" type="text" required placeholder="Nama Kategori Barang">
                            </div>
                            <div class="mt-4">
                                <label class="block text-base text-gray-600 font-semibold" for="description">Deskripsi Singkat</label>
                                <input class="w-full px-5 py-3 transition duration-300 border border-gray-300 rounded focus:border-transparent focus:outline-none focus:ring-4 focus:ring-blue-200" id="description" name="description" type="text" placeholder="Deskripsi Kategori">
                            </div>
                            <div class="inline-block mr-2 mt-6">
                                <button type="submit" class="focus:outline-none font-light tracking-wider text-white py-2 px-5 rounded-md bg-blue-500 hover:bg-blue-600 hover:shadow-lg">Simpan</button>
                             </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
