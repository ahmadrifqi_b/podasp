<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Detail Data Pemasok') }}
        </h2>
    </x-slot>

    <div class="py-10">
        <div class="w-3/4 mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg border-lg border">
                <div class="w-full lg:w-3/4 m-5 pr-0 lg:pr-2">
                    <div class="leading-loose">
                        <form action="/pemasok/update/{{$data->id}}" method="post" class="p-0 lg:py-5 lg:px-10">
                            @method('PUT')
                            @csrf
                            <div class="mt-4">
                                <label class="block text-base text-gray-600 font-semibold" for="supplier_name">Nama</label>
                                <input class="w-full px-5 py-3 transition duration-300 border border-gray-300 rounded focus:border-transparent focus:outline-none focus:ring-4 focus:ring-blue-200" id="supplier_name" name="supplier_name" type="text" required placeholder="Nama Pemasok" value="{{ $data->supplier_name }}">
                            </div>
                            <div class="mt-4">
                                <label class="block text-base text-gray-600 font-semibold" for="address">Alamat</label>
                                <textarea class="w-full px-5 py-3 transition duration-300 border border-gray-300 rounded focus:border-transparent focus:outline-none focus:ring-4 focus:ring-blue-200" id="address" name="address" type="text" placeholder="Alamat Pemasok">{{ $data->address }}</textarea>
                            </div>
                            <div class="mt-4">
                                <label class="block text-base text-gray-600 font-semibold" for="telephone">Telepon</label>
                                <input class="w-full px-5 py-3 transition duration-300 border border-gray-300 rounded focus:border-transparent focus:outline-none focus:ring-4 focus:ring-blue-200" id="telephone" name="telephone" type="text" placeholder="Nomor Telepon" value="{{ $data->telephone }}">
                            </div>
                            <div class="inline-block mr-2 mt-6">
                                <button type="submit" class="focus:outline-none font-light tracking-wider text-white py-2 px-5 rounded-md bg-blue-500 hover:bg-blue-600 hover:shadow-lg">Simpan</button>
                             </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
