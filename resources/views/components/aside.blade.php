<aside class="relative bg-sidebar h-screen w-64 hidden sm:block shadow-xl">
    <div class="p-6">
        <a href="{{ route('dashboard') }}" class="text-white text-3xl font-semibold uppercase hover:text-gray-300">Panel</a>
        
    </div>
    <nav class="text-white text-base font-semibold pt-3">
        <a href="{{ route('dashboard') }}" class="{{ request()->is('dashboard') ? 'active-nav-link' : 'opacity-75 hover:opacity-100' }} flex items-center text-white py-4 pl-6 nav-item">
            <i class="fas fa-tachometer-alt mr-3"></i>
            Dashboard
        </a>
        <a href="{{ route('formulir.index') }}" class="{{ request()->is('formulir') ? 'active-nav-link' : 'opacity-75 hover:opacity-100' }} flex items-center text-white py-4 pl-6 nav-item">
            <i class="fas fa-book mr-3"></i>
            Purchase Order
        </a>
        <a href="{{ route('apriori') }}" class="{{ request()->is('algorithms') ? 'active-nav-link' : 'opacity-75 hover:opacity-100' }} flex items-center text-white py-4 pl-6 nav-item">
            <i class="fas fa-chart-bar mr-3"></i>
            Data Mining
        </a>
        @if (Auth::user()->role_id == 3 || Auth::user()->role_id == 1)
        <a href="{{ route('kategori.index') }}" class="{{ request()->is('kategori') ? 'active-nav-link' : 'opacity-75 hover:opacity-100' }} flex items-center text-white py-4 pl-6 nav-item">
            <i class="fas fa-boxes mr-3"></i>
            Jenis Barang
        </a>
        @endif
        @if (Auth::user()->role_id == 3 || Auth::user()->role_id == 1)
        <a href="{{ route('pemasok.index') }}" class="{{ request()->is('pemasok.index') ? 'active-nav-link' : 'opacity-75 hover:opacity-100' }} flex items-center text-white py-4 pl-6 nav-item">
            <i class="fas fa-truck-loading mr-3"></i>
            Daftar Pemasok
        </a>
        @endif
        @if (Auth::user()->role_id == 1)
        <a href="{{ route('karyawan.index') }}" class="{{ request()->is('karyawan.index') ? 'active-nav-link' : 'opacity-75 hover:opacity-100' }} flex items-center text-white py-4 pl-6 nav-item">
            <i class="fas fa-user-friends mr-3"></i>
            Daftar Karyawan
        </a>
        @endif
    </nav>
</aside>