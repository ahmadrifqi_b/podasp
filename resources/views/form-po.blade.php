<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Formulir Purchase Order') }}
        </h2>
    </x-slot>

    {{-- Style Table --}}
    @push('style')
    <link href="https://unpkg.com/tailwindcss/dist/tailwind.min.css" rel="stylesheet">
	<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
	<link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet">
    <style>
		
		/*Overrides for Tailwind CSS */
		
		/*Form fields*/
		.dataTables_wrapper select,
		.dataTables_wrapper .dataTables_filter input {
			color: #4a5568; 			/*text-gray-700*/
			padding-left: 1rem; 		/*pl-4*/
			padding-right: 1rem; 		/*pl-4*/
			padding-top: .5rem; 		/*pl-2*/
			padding-bottom: .5rem; 		/*pl-2*/
			line-height: 1.25; 			/*leading-tight*/
			border-width: 2px; 			/*border-2*/
			border-radius: .25rem; 		
			border-color: #edf2f7; 		/*border-gray-200*/
			background-color: #edf2f7; 	/*bg-gray-200*/
		}

		/*Row Hover*/
		table.dataTable.hover tbody tr:hover, table.dataTable.display tbody tr:hover {
			background-color: #ebf4ff;	/*bg-indigo-100*/
		}
		
		/*Pagination Buttons*/
		.dataTables_wrapper .dataTables_paginate .paginate_button		{
			font-weight: 700;				/*font-bold*/
			border-radius: .25rem;			/*rounded*/
			border: 1px solid transparent;	/*border border-transparent*/
		}
		
		/*Pagination Buttons - Current selected */
		.dataTables_wrapper .dataTables_paginate .paginate_button.current	{
			color: #fff !important;				/*text-white*/
			box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06); 	/*shadow*/
			font-weight: 700;					/*font-bold*/
			border-radius: .25rem;				/*rounded*/
			background: #667eea !important;		/*bg-indigo-500*/
			border: 1px solid transparent;		/*border border-transparent*/
		}

		/*Pagination Buttons - Hover */
		.dataTables_wrapper .dataTables_paginate .paginate_button:hover		{
			color: #fff !important;				/*text-white*/
			box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);	 /*shadow*/
			font-weight: 700;					/*font-bold*/
			border-radius: .25rem;				/*rounded*/
			background: #667eea !important;		/*bg-indigo-500*/
			border: 1px solid transparent;		/*border border-transparent*/
		}
		
		/*Add padding to bottom border */
		table.dataTable.no-footer {
			border-bottom: 1px solid #e2e8f0;	/*border-b-1 border-gray-300*/
			margin-top: 0.75em;
			margin-bottom: 0.75em;
		}
		
		/*Change colour of responsive icon*/
		table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before {
			background-color: #667eea !important; /*bg-indigo-500*/
		}
		
      </style>
    @endpush

    <div class="py-10">
        <div class="w-full mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg border-lg border">
                <div class="w-full p-10 bg-white rounded shadow-xl">
                    @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
                    <div class="flex justify-between my-5">
                        <p class="text-xl pb-3 flex items-center">
                            <i class="fas fa-list mr-3"></i> Daftar Purchase Order
                        </p>
                        <div class="">
                            <a href="/formulir/create" class="px-4 py-2 text-white font-light tracking-wider focus:outline-none rounded-md bg-blue-500 hover:bg-blue-600 hover:shadow-lg">Tambah Data</a>
                        </div>
                    </div>
                    @endif

					{{-- Khusus Purchasing --}}
                    @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
                    <div class="p-8 mt-6 lg:mt-0 rounded shadow bg-white overflow-auto rounded-lg border border-2 border-gray-200">
                        <table id="tabledata" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
                            <thead>
                                <tr>
                                    <th data-priority="1" class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">No</th>
                                    <th data-priority="2" class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Nama Formulir</th>
                                    <th data-priority="3" class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Tanggal</th>
                                    <th data-priority="3" class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Status</th>
                                    <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; ?>
                                @foreach ($data as $data)
                                @if ($data->director_confirmed_id !=3 )
                                    <tr class="hover:bg-grey-lighter">
                                        <td class="py-4 px-6 border-b border-grey-light">{{ $no++ }}</td>
                                        <td class="py-4 px-6 border-b border-grey-light">{{ $data->project_name }}</td>
                                        <td class="py-4 px-6 border-b border-grey-light">{{ $data->date }}</td>
                                        <td class="py-4 px-6 border-b border-grey-light">{{ $data->director->name }}</td>
                                        <td class="py-4 px-6 border-b border-grey-light">
                                            <div class="inline-block mr-2 mt-2">
                                                <a href="{{ route('formulir.show', $data->id) }}" class="focus:outline-none text-white py-2 px-4 rounded-md bg-green-500 hover:bg-green-600 hover:shadow-lg">Lihat</a>
                                            </div>
                                            <div class="inline-block mr-2 mt-2">
                                                <!-- modal div -->
                                                <div x-data="{ open: false }">
                                                    <button type="button" class="focus:outline-none text-white py-2 px-4 rounded-md bg-red-500 hover:bg-red-600 hover:shadow-lg" @click="open = true">Hapus</button>
                                                    <!-- Dialog (full screen) -->
                                                    <div class="absolute top-0 left-0 flex items-center justify-center w-full h-full" style="background-color: rgba(0,0,0,.5);" x-show="open"  >
                                                    <!-- A basic modal dialog with title, body and one button to close -->
                                                        <div class="h-auto p-4 mx-2 text-left bg-white rounded shadow-xl md:max-w-xl md:p-6 lg:p-8 md:mx-0" @click.away="open = false">
                                                            <div class="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                                                                <h3 class="text-lg font-high leading-6 text-black">
                                                                    Perhatian !!
                                                                </h3>
                                                    
                                                                <div class="mt-2">
                                                                    <p class="text-sm leading-5 text-gray-500">
                                                                    Apakah anda yakin ingin menghapus data ini ?
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <!-- One big close button.  --->
                                                            <div class="mt-5 sm:mt-6">
                                                                <span class="flex w-full rounded-md shadow-sm">
                                                                    <button @click="open = false" class="inline-flex justify-center w-full mx-5 px-4 py-2 text-white bg-blue-500 rounded hover:bg-blue-700">
                                                                    Tidak
                                                                    </button>
                                                                    <a href="{{ route('formulir.delete', $data->id) }}" class="inline-flex justify-center w-full mx-5 px-4 py-2 text-white bg-red-500 rounded hover:bg-red-700">
                                                                        Ya
                                                                    </a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif

                    @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
                    <div class="flex justify-between mt-20">
                        <p class="text-xl pb-3 flex items-center">
                            <i class="fas fa-list mr-3"></i> Daftar Purchase Order Sudah di Terima
                        </p>
                    </div>
                    <div class="p-8 mt-6 lg:mt-10 rounded shadow bg-white overflow-auto rounded-lg border border-2 border-gray-200">
                        <table id="fixdata" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
                            <thead>
                                <tr>
                                    <th data-priority="1" class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">No</th>
                                    <th data-priority="2" class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Nama Formulir</th>
                                    <th data-priority="3" class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Tanggal</th>
                                    <th data-priority="3" class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; ?>
                                @foreach ($dataacc as $data)
                                @if ($data->director_confirmed_id == 3 )
                                    <tr class="hover:bg-grey-lighter">
                                        <td class="py-4 px-6 border-b border-grey-light">{{ $no++ }}</td>
                                        <td class="py-4 px-6 border-b border-grey-light">{{ $data->project_name }}</td>
                                        <td class="py-4 px-6 border-b border-grey-light">{{ $data->date }}</td>
                                        <td class="py-4 px-6 border-b border-grey-light">{{ $data->director->name }}</td>
                                    </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif

                    @if (Auth::user()->role_id != 3)
                    <div class="flex justify-between mt-20">
                        <p class="text-xl pb-3 flex items-center">
                            <i class="fas fa-list mr-3"></i> Daftar Purchase Order
                        </p>
                    </div>
					{{-- Khusus Direktur dan bendahara --}}
					<div class="p-8 mt-6 lg:mt-10 rounded shadow bg-white overflow-auto rounded-lg border border-2 border-gray-200">
                        <table id="docdata" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
                            <thead>
                                <tr>
                                    <th data-priority="1" class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">No</th>
                                    <th data-priority="2" class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Nama Formulir</th>
                                    <th data-priority="3" class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Tanggal</th>
                                    <th data-priority="3" class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Status PO</th>
                                    <th data-priority="3" class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Status Keuangan</th>
                                    <th class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; ?>
                                @foreach ($item as $data)
                                <tr class="hover:bg-grey-lighter">
                                    <td class="py-4 px-6 border-b border-grey-light">{{ $no++ }}</td>
                                    <td class="py-4 px-6 border-b border-grey-light">{{ $data->project_name }}</td>
                                    <td class="py-4 px-6 border-b border-grey-light">{{ $data->date }}</td>
                                    <td class="py-4 px-6 border-b border-grey-light">{{ $data->director->name }}</td>
                                    <td class="py-4 px-6 border-b border-grey-light">{{ $data->finance->name }}</td>
                                    <td class="py-4 px-6 border-b border-grey-light">
                                        <div class="inline-block mr-2 mt-2">
                                            <a href="{{ route('formulir.show', $data->id) }}" class="focus:outline-none text-white py-2 px-4 rounded-md bg-green-500 hover:bg-green-600 hover:shadow-lg">Lihat</a>
                                         </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @push('script')
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
	<script>
		$(document).ready(function() {
			var table = $('#tabledata').DataTable( {
					responsive: true
				} )
				.columns.adjust()
				.responsive.recalc();
		} );
	</script>
    <script>
		$(document).ready(function() {
			var table = $('#fixdata').DataTable( {
					responsive: true
				} )
				.columns.adjust()
				.responsive.recalc();
		} );
	</script>
	<script>
		$(document).ready(function() {
			var table = $('#docdata').DataTable( {
					responsive: true
				} )
				.columns.adjust()
				.responsive.recalc();
		} );
	</script>
	<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.8.2/dist/alpine.min.js" defer></script>
    @endpush
</x-app-layout>
