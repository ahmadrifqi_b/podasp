<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Data Mining') }}
        </h2>
    </x-slot>

    @push('style')
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    @endpush

    <div class="py-12">
        <div class="w-full mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg border">
                <div class="w-full p-10 bg-white rounded shadow-xl antialiased">
                    {{-- Button Trigger --}}
                    <div class="mb-10">
                        <form class="flex w-full max-w-5xl">
                            <div class="w-2/5">
                                <div class="md:flex md:items-center mb-6">
                                    <div class="md:w-2/4">
                                        <label class="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" for="support">
                                            Support
                                        </label>
                                    </div>
                                    <div class="md:w-1/4">
                                        <input class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" type="number" max='100' min=0 value='{{ \request()->support ?? 1 }}' name='support' for='support' placeholder='Support'>
                                    </div>
                                    <div class="md:w-1/4 block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pl-4">%</div>
                                </div>
                                <div class="md:flex md:items-center mb-6">
                                    <div class="md:w-2/4">
                                        <label class="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" for="confidence">
                                            Confidence
                                        </label>
                                    </div>
                                    <div class="md:w-1/4">
                                        <input class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" type="number" max='100'  min=0  value='{{ \request()->confidence ?? 1 }}' name='confidence' for='confidence' placeholder='Confidence'>
                                    </div>
                                    <div class="md:w-1/4 block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pl-4">%</div>
                                </div>
                                <div class="md:flex md:items-center">
                                    <div class="md:w-2/4"></div>
                                    <div class="md:w-2/4">
                                        <button class="shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" type="submit">
                                            Hitung Apriori
                                        </button>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="w-3/5">
                                <div class="md:flex md:items-center mb-6">
                                    <div class="md:w-1/6">
                                        <label class="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" for="support">
                                            Tanggal
                                        </label>
                                    </div>
                                    <div class="md:w-4/6">
                                        <input class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" id='daterangepicker' type="text"  value='{{ \request()->tanggal_transaksi ?? 1 }}' name='tanggal_transaksi' placeholder='Tanggal Transaksi'>
                                    </div>
                                    <div class="md:w-2/6"></div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <hr>

                    @if(\request()->support && \request()->confidence)
                    <div class="mt-10 block text-gray-500 font-bold text-3xl">Support A</div>
                    {{-- <table class='table-auto'>
                        <thead>
                            <tr>
                                <td>No</td>
                                @foreach($types as $type)
                                <td>{{ $type->type_name }}</td>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($forms as $form)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    @foreach($types as $type)
                                    <td>{{ $form->form->where('type_data_id', $type->id)->count() }}</td>
                                    @endforeach
                                </tr>
                            @endforeach

                                <tr>
                                    <td></td>
                                    @foreach($types as $type)
                                    <td>{{ $formDatas->where("type_data_id", $type->id)->count() }}</td>
                                    @endforeach
                                </tr>
                        </tbody>
                    </table> --}}
                     
                    <div class="mt-5 mb-10">
                        <table class="table-auto border-collapse border-2 rounded-lg border-black border-opacity-50">
                            <thead>
                                <tr>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">No</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">Jenis / Kategori Barang</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">Support</td>
                                    {{-- <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">Confidence</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">Support x Confidence</td> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                
                                @foreach($types as $type)
                                @php 
                                try {
                                    ($formDatas->where("type_data_id", $type->id)->count() . "/" . $formDatas->where("type_data_id", $type->id)->count()) . " X " . "100% = " . ($formDatas->where("type_data_id", $type->id)->count() /  $formDatas->where("type_data_id", $type->id)->count() * 100) . "%";

                                    $support = $formDatas->where("type_data_id", $type->id)->count() /  $forms->count() * 100;
                                    $confidence = $formDatas->where("type_data_id", $type->id)->count() /  $formDatas->where("type_data_id", $type->id)->count() * 100;

                                    if(!($support >= \request()->support)):
                                    // && $confidence >= \request()->confidence
                                    continue;
                                    endif;
                                } catch (\Throwable $t) {
                                    continue;
                                }
                                @endphp
                                    <tr>
                                        <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ $no++ }}</td>
                                        <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ $type->type_name }}</td>
                                        <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ ($formDatas->where("type_data_id", $type->id)->count() . "/" . $forms->count()) . " X " . "100% = " . ($formDatas->where("type_data_id", $type->id)->count() /  $forms->count() * 100) . "%" }}</td>
                                        {{-- <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ ($formDatas->where("type_data_id", $type->id)->count() . "/" . $formDatas->where("type_data_id", $type->id)->count()) . " X " . "100% = " . ($formDatas->where("type_data_id", $type->id)->count() /  $formDatas->where("type_data_id", $type->id)->count() * 100) . "%" }}</td>
                                        <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ ($formDatas->where("type_data_id", $type->id)->count() /  $forms->count()) *  ($formDatas->where("type_data_id", $type->id)->count() /  $formDatas->where("type_data_id", $type->id)->count() * 100) . "%"}}</td> --}}
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>  

                    {{-- <table class='table-auto'>
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Kategori</td>
                                <td>Jumlah</td>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach($types as $key => $type)
                            @php
                                $form = $forms->toArray()[$key];
                            @endphp

                            @foreach($types as $item)
                            <tr>
                                
                                @if($item->type_name != $type->type_name)
                                <td>{{ $no++ }}</td>
                                <td>{{ $type->type_name }},{{ $item->type_name }}</td>
                                <td>
                                    @php
                                        $form_data_form_ids = $formDatas->unique("form_id")->pluck("form_id")->toArray();
                                        $total = 0;
                                        foreach ($form_data_form_ids as $form_data_form_id):
                                            if($formDatas->where("type_data_id", $type->id)->where("form_id", $form_data_form_id)->count() && $formDatas->where("form_id", $form_data_form_id)->where("type_data_id", $item->id)->count()) {
                                                $total++;
                                            }
                                        endforeach
                                    @endphp
                                    {{ $total }}
                                </td>
                                @endif

                            </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table> --}}

                    <hr>

                    <div class="mt-10 text-3xl block text-gray-500 font-bold">Support A dan B</div>

                    <div class="mt-5">
                        <table class='table-auto border-collapse border-2 rounded-lg border-black border-opacity-50'>
                            <thead>
                                <tr>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">No</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">Jenis / Kategori Barang</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">Support</td>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($types as $key => $type)
    
                                @foreach($types as $item)
                                @php
                                    $form_data_form_ids = $formDatas->unique("form_id")->pluck("form_id")->toArray();
                                    $total = 0;
                                    foreach ($form_data_form_ids as $form_data_form_id):
                                        if($formDatas->where("type_data_id", $type->id)->where("form_id", $form_data_form_id)->count() && $formDatas->where("form_id", $form_data_form_id)->where("type_data_id", $item->id)->count()) {
                                            $total++;
                                        }
    
                                    endforeach;
                                    try {
                                        if($total < 2) break;
    
                                        $support = $total /  $forms->count() * 100;
                                        $confidence = round($total /  $formDatas->where("type_data_id", $type->id)->count() * 100, 2);
                                        if(!($support >= \request()->support)): break; endif;
                                    } catch(Throwable $t) {
                                        continue;
                                    }
                                @endphp
    
                                <tr>    
                                    @if($item->type_name != $type->type_name)
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ $no++ }}</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ $type->type_name }},{{ $item->type_name }}</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ ($total . "/" . $forms->count()) . " X " . "100% = " . ($total /  $forms->count() * 100) . "%" }}</td>
                                    @endif
    
                                </tr>
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    

                    <div class="mt-8 text-3xl block text-gray-500 font-bold">Confidence A dan B</div>
                    <div class="mt-5 mb-10">
                        <table class="table-auto border-collapse border-2 rounded-lg border-black border-opacity-50">
                            <thead>
                                <tr>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">No</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">Jenis / Kategori Barang</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">Support</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">Confidence</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">Support x Confidence</td>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($types as $key => $type)

                                @foreach($types as $item)
                                @php
                                    $form_data_form_ids = $formDatas->unique("form_id")->pluck("form_id")->toArray();
                                    $total = 0;
                                    foreach ($form_data_form_ids as $form_data_form_id):
                                        if($formDatas->where("type_data_id", $type->id)->where("form_id", $form_data_form_id)->count() && $formDatas->where("form_id", $form_data_form_id)->where("type_data_id", $item->id)->count()) {
                                            $total++;
                                        }

                                    endforeach;
                                    try {
                                        if($total < 2) break;

                                        $support = $total /  $forms->count() * 100;
                                        $confidence = round($total /  $formDatas->where("type_data_id", $type->id)->count() * 100, 2);
                                        if(!($support >= \request()->support && $confidence >= \request()->confidence)): break; endif;
                                    } catch(Throwable $t) {
                                        continue;
                                    }
                                @endphp

                                <tr>    
                                    @if($item->type_name != $type->type_name)
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ $no++ }}</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ $type->type_name }}, {{ $item->type_name }}</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ ($total . "/" . $forms->count()) . " X " . "100% = " . ($total /  $forms->count() * 100) . "%" }}</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ ($total . "/" . $formDatas->where("type_data_id", $type->id)->count()) . " X " . "100% = " . (round($total /  $formDatas->where("type_data_id", $type->id)->count() * 100, 2)) . "%" }}</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ round(($total /  $forms->count()) *  ($total /  $formDatas->where("type_data_id", $type->id)->count() * 100), 2) . "%"}}</td>
                                    @endif
                                </tr>
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    
                    {{-- <div style='margin-top: 20px;'></div>
                    <h1>Support L3</h1> --}}
                    {{-- <table class='table-auto'>
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Kategori</td>
                                <td>Jumlah</td>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach($types as $key => $type1)
                                @foreach($types as $key => $type2)
                                @php
                                    $form = $forms->toArray()[$key];

                                    if($type2->type_name == $type1->type_name) continue;
                                @endphp

                                @foreach($types as $type3)
                                <tr>
                                    
                                    @if(($type3->type_name != $type2->type_name) && ($type3->type_name != $type1->type_name))
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $type1->type_name }},{{ $type2->type_name }},{{ $type3->type_name }}</td>
                                    <td>
                                        @php
                                            $form_data_form_ids = $formDatas->unique("form_id")->pluck("form_id")->toArray();
                                            $total = 0;
                                            foreach ($form_data_form_ids as $form_data_form_id):
                                                if($formDatas->where("type_data_id", $type1->id)->where("form_id", $form_data_form_id)->count() && $formDatas->where("form_id", $form_data_form_id)->where("type_data_id", $type2->id)->count() && $formDatas->where("type_data_id", $type3->id)->where("form_id", $form_data_form_id)->count()) {
                                                    $total++;
                                                }
                                            endforeach
                                        @endphp
                                        {{ $total }}
                                    </td>
                                    @endif

                                </tr>
                                    @endforeach
                                @endforeach
                            @endforeach
                        </tbody>
                    </table> --}}

                    <hr>

                    <div class="mt-10 block text-gray-500 font-bold text-3xl">Support A dan B dan C</div>
                    <div class="mt-5">
                        <table class="table-auto border-collapse border-2 rounded-lg border-black border-opacity-50">
                            <thead>
                                <tr>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">No</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">Jenis / Kategori Barang</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">Support</td>
                                    {{-- <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">Confidence</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">Support x Confidence</td> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($types as $key1 => $type1)
                                    @foreach($types as $key => $type)
                                        @foreach($types as $item)
                                        @php
                                            $form_data_form_ids = $formDatas->unique("form_id")->pluck("form_id")->toArray();
                                            $total = 0;
                                            foreach ($form_data_form_ids as $form_data_form_id):
                                                if($formDatas->where("type_data_id", $type1->id)->where("form_id", $form_data_form_id)->count() && $formDatas->where("type_data_id", $type->id)->where("form_id", $form_data_form_id)->count() && $formDatas->where("form_id", $form_data_form_id)->where("type_data_id", $item->id)->count()) {
                                                    $total++;
                                                }

                                            endforeach;
                                            try {
                                                if($total < 3) break;

                                                $support = $total /  $forms->count() * 100;
                                                $confidence = round($total /  $formDatas->where("type_data_id", $type->id)->count() * 100, 2);
                                                if(!($support >= \request()->support)): break; endif;
                                                // && $confidence >= \request()->confidence
                                            } catch(Throwable $t) {
                                                continue;
                                            }
                                        @endphp

                                        <tr>    
                                            @if(($item->type_name != $type->type_name) && ($item->type_name != $type1->type_name) && ($type1->type_name != $type->type_name))
                                                <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ $no++ }}</td>
                                                <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ $type1->type_name }},{{ $type->type_name }},{{ $item->type_name }}</td>
                                                <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ ($total . "/" . $forms->count()) . " X " . "100% = " . ($total /  $forms->count() * 100) . "%" }}</td>
                                                {{-- <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ ($total . "/" . $formDatas->where("type_data_id", $type->id)->count()) . " X " . "100% = " . (round($total /  $formDatas->where("type_data_id", $type->id)->count() * 100, 2)) . "%" }}</td>
                                                <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ round(($total /  $forms->count()) *  ($total /  $formDatas->where("type_data_id", $type->id)->count() * 100), 2) . "%"}}</td> --}}
                                            @endif
                                        </tr>
                                        @endforeach
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="mt-8 block text-gray-500 font-bold text-3xl">Confidence A dan B dan C</div>
                    <div class="mt-5 mb-10">
                        <table class="table-auto border-collapse border-2 rounded-lg border-black border-opacity-50">
                            <thead>
                                <tr>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">No</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">Jenis / Kategori Barang</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">Support</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">Confidence</td>
                                    <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-2 text-xl">Support x Confidence</td>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($types as $key1 => $type1)
                                    @foreach($types as $key => $type)
                                        @foreach($types as $item)
                                        @php
                                            $form_data_form_ids = $formDatas->unique("form_id")->pluck("form_id")->toArray();
                                            $total = 0;
                                            foreach ($form_data_form_ids as $form_data_form_id):
                                                if($formDatas->where("type_data_id", $type1->id)->where("form_id", $form_data_form_id)->count() && $formDatas->where("type_data_id", $type->id)->where("form_id", $form_data_form_id)->count() && $formDatas->where("form_id", $form_data_form_id)->where("type_data_id", $item->id)->count()) {
                                                    $total++;
                                                }

                                            endforeach;
                                            try {
                                                if($total < 3) break;

                                                $support = $total /  $forms->count() * 100;
                                                $confidence = round($total /  $formDatas->where("type_data_id", $type->id)->count() * 100, 2);
                                                if(!($support >= \request()->support && $confidence >= \request()->confidence)): break; endif;
                                            } catch(Throwable $t) {
                                                continue;
                                            }
                                        @endphp

                                        <tr>    
                                            @if(($item->type_name != $type->type_name) && ($item->type_name != $type1->type_name) && ($type1->type_name != $type->type_name))
                                                <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ $no++ }}</td>
                                                <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ $type1->type_name }},{{ $type->type_name }},{{ $item->type_name }}</td>
                                                <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ ($total . "/" . $forms->count()) . " X " . "100% = " . ($total /  $forms->count() * 100) . "%" }}</td>
                                                <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ ($total . "/" . $formDatas->where("type_data_id", $type->id)->count()) . " X " . "100% = " . (round($total /  $formDatas->where("type_data_id", $type->id)->count() * 100, 2)) . "%" }}</td>
                                                <td class="border-2 border-black border-opacity-50 font-semibold px-4 py-1 text-md">{{ round(($total /  $forms->count()) *  ($total /  $formDatas->where("type_data_id", $type->id)->count() * 100), 2) . "%"}}</td>
                                            @endif
                                        </tr>
                                        @endforeach
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <hr>
                    @endif

                </div>
            </div>
        </div>
    </div>
    @push('script')
        <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <script>
            $(document).ready(function() {
                $('#daterangepicker').daterangepicker({
                    locale: {
                        format: 'Y-MM-DD'
                    }
                });
            })
        </script>
    @endpush
</x-app-layout>
