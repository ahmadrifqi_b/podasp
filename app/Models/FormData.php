<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormData extends Model
{
    use HasFactory;

    protected $fillable = [
        'form_id',
        'type_data_id',
        'supplier_id',
        'item_name',
        'unit',
        'quantity',
        'price'
    ];

    public function form()
    {
        return $this->belongsTo(Form::class);
    }

    public function category()
    {
        return $this->belongsTo(TypeData::class, 'type_data_id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }
}
