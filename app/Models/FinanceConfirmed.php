<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinanceConfirmed extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function form()
    {
        return $this->hasMany(Form::class);
    }
}
