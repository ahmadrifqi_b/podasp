<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'po_number',
        'project_name',
        'address',
        'delivery',
        'email',
        'date',
        'telephone',
        'fax',
        'director_confirmed_id',
        'finance_confirmed_id'
    ];

    public function form()
    {
        return $this->hasMany(FormData::class, 'form_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'user_id');
    }

    public function director()
    {
        return $this->belongsTo(DirectorConfirmed::class, 'director_confirmed_id');
    }

    public function finance()
    {
        return $this->belongsTo(FinanceConfirmed::class, 'finance_confirmed_id');
    }
}
