<?php

namespace App\Http\Controllers;

use App\Models\{Form, DirectorConfirmed, FinanceConfirmed};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FormController extends Controller
{
  public function __construct()
  {
    $this->Form = new Form();
  }

  public function index()
  {
    $data = $this->Form->get();
    return view('form-po', [
      'data' => $data,
      'dataacc' => $data, 
      'item' => $data
    ]);
  }

  public function create()
  {
    return view("components.form.create");
  }

  public function store(Request $request)
  {
    $request->validate([
      'project_name' => 'required',
    ]);

    $data = [
      'user_id' => Auth::user()->id,
      'po_number' => Request()->po_number,
      'project_name' => Request()->project_name,
      'address' => Request()->address,
      'delivery' => Request()->delivery,
      'email' => Request()->mail,
      'date' => Request()->date,
      'telephone' => Request()->telephone,
      'fax' => Request()->fax,
      'director_confirmed_id' => Request()->director_confirmed_id,
      'finance_confirmed_id' => Request()->finance_confirmed_id
    ];

    $formData = Form::create($data);
    return redirect()->route('formulir.show', $formData->id);
  }

    public function show($id)
    {
      $data = Form::where('id', $id)->first();
      return view('components.form.show', ['data' => $data]);
    }

    public function edit($id)
    {
      $data = Form::where('id', $id)->first();
      return view('components.form.edit', ['data' => $data]);
    }

    public function update(Request $request, $id)
    {
      $data = Form::find($id);
      $request->validate([
        'project_name' => 'required',
      ]);

      $data->po_number = Request()->po_number;
      $data->project_name = Request()->project_name;
      $data->address = Request()->address;
      $data->delivery = Request()->delivery;
      $data->email = Request()->mail;
      $data->date = Request()->date;
      $data->telephone = Request()->telephone;
      $data->fax = Request()->fax;
      $data->director_confirmed_id = Request()->director_confirmed_id;
      $data->finance_confirmed_id = Request()->finance_confirmed_id;
      
      $data->save();
      return redirect()->route('formulir.index');
    }

    public function destroy($id)
    {
      Form::destroy($id);
      return redirect()->route('formulir.index');
    }

  // For Director
  public function editdirector($id)
  {
    $data = Form::where('id', $id)->first();
    return view('components.form.director.update', [
      'data' => $data,
      'director' => DirectorConfirmed::get()
      ]);
  }

  public function updatedirector(Request $request, $id)
  {
    $data = Form::find($id);
    $data->director_confirmed_id = Request()->director_confirmed_id;
    
    $data->save();
    return redirect()->route('formulir.index');
  }

  // For Finance
  public function editfincance($id)
  {
    $data = Form::where('id', $id)->first();
    return view('components.form.finance.update', [
      'data' => $data, 
      'finance' => FinanceConfirmed::get()
    ]);
  }

  public function updatefinance(Request $request, $id)
  {
    $data = Form::find($id);
    $data->finance_confirmed_id = Request()->finance_confirmed_id;
    
    $data->save();
    return redirect()->route('formulir.index');
  }

  public function apriori()
  {
    $data = $this->Form->get();
    return view('algoritms', [
      'data' => $data,
      'dataacc' => $data, 
      'item' => $data
    ]);
    // $data = Form::where()->get();
    // $firstData = Form::get()->first();
    // $lastData = Form::latest()->first();
    //dd($data);
    // while($firstData->id < $lastData->id){
    //   dd($data->project_name);
    // }
    
    // foreach($data as $item){
      
    // }

    // $i=1;
    // while($i < 10){
    //   echo "$i";
    //   $i++;
    // }
  }
}
