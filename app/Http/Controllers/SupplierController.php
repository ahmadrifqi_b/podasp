<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Supplier;

class SupplierController extends Controller
{
    public function __construct()
    {
        $this->Supplier = new Supplier();
    }
    
    public function index()
    {
        //$suppliers = DB::table('suppliers')->get();

        $data = $this->Supplier->get();
        return view('supplier', ['suppliers' => $data]);
    }

    public function create()
    {
        return view('components.supplier.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'supplier_name' => 'required',
        ]);

        $data = [
            'supplier_name' => Request()->supplier_name,
            'address' => Request()->address,
            'telephone' => Request()->telephone
        ];

        Supplier::create($data);
        return redirect()->route('pemasok.index');
    }


    public function show($id)
    {
        //$data = DB::table('suppliers')->where('id', $id)->first();
        $data = Supplier::where('id', $id)->first();
        return view('components.supplier.show', ['data' => $data]);
        //return view('components.supplier.show', compact('supplier'));
    }

    public function update(Request $request, $id)
    {
        $data = Supplier::find($id);
        $request->validate([
            'supplier_name' => 'required',
        ]);

        $data->supplier_name = Request()->supplier_name;
        $data->address = Request()->address;
        $data->telephone = Request()->telephone;
        
        $data->save();
        return redirect()->route('pemasok');

        //$data = Supplier::where('id', $id)->first();
        //DB::table('suppliers')->where('id', $id)->update($data);

        

        //Supplier::save($data);
        
        //$a = $supplier->update($data);
        //dd($a);

        // $request->validate([
        //     'supplier_name' => 'required',
        // ]);

        // $supplier->update($request->all());

        
    }

    public function destroy($id)
    {
        Supplier::destroy($id);
        return redirect()->route('pemasok');
    }
}
