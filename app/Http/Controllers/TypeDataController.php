<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\TypeData;

class TypeDataController extends Controller
{
    public function __construct()
    {
        $this->TypeData = new TypeData();
    }
    
    public function index()
    {
        $data = $this->TypeData->get();
        return view('type-item', ['category' => $data]);
    }

    public function create()
    {
        return view('components.item.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'type_name' => 'required',
        ]);

        $data = [
            'type_name' => Request()->type_name,
            'description' => Request()->description,
        ];

        TypeData::create($data);
        return redirect()->route('kategori.index');
    }


    public function show($id)
    {
        $data = TypeData::where('id', $id)->first();
        return view('components.item.show', ['data' => $data]);
    }

    public function update(Request $request, $id)
    {
        $data = TypeData::find($id);
        $request->validate([
            'type_name' => 'required',
        ]);

        $data->type_name = Request()->type_name;
        $data->description = Request()->description;
        
        $data->save();
        return redirect()->route('kategori');
    }

    public function destroy($id)
    {
        TypeData::destroy($id);
        return redirect()->route('kategori');
    }
}
