<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    public function __construct()
    {
        $this->User = new User();
    }
    public function index()
    {
        $data = $this->User->get();
        return view('employee', ['user' => $data]);
    }

    public function create()
    {
        return view("components.user.create");
    }

    public function store(Request $request)
    {
        $data = [
            'name' => Request()->name,
            'email' => Request()->email,
            'password' => \Hash::make(Request()->password),
            'role_id' => Request()->role_id
        ];

        User::create($data);
        return redirect()->route('karyawan.index');
    }

    // public function destroy($id)
    // {
    //     User::destroy($id);
    //     return redirect()->route('karyawan.index');
    // }
}
