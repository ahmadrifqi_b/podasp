<?php

namespace App\Http\Controllers;

use App\Models\{Form, FormData, TypeData, Supplier};
use Illuminate\Http\Request;

class FormDataController extends Controller
{
    public function __construct()
    {
        $this->FormData = new FormData();
    }

    public function index()
    {
        $data = $this->Form->get();
        return view('components.form.show', ['data' => $data]);
    }

    public function create(Form $id)
    {
        return view("components.form.item.create", [
            'form' => $id,
            'category' => TypeData::get(),
            'supplier' => Supplier::get()
        ]);
    }

    public function store(Request $request, $id)
    {
        $request->validate([
            'item_name' => 'required',
        ]);
      
        $data = [
            'form_id' => Request()->form_id,
            'type_data_id' => Request()->type_data_id,
            'supplier_id' => Request()->supplier_id,
            'item_name' => Request()->item_name,
            'unit' => Request()->unit,
            'quantity' => Request()->quantity,
            'price' => Request()->price
        ];
      
        FormData::create($data);
        return redirect()->route('formulir.show', $id);
    }

    public function show($id)
    {
        $data = FormData::where('id', $id)->first();
        return view('components.form.item.show', [
            'data' => $data,
            'category' => TypeData::get(),
            'supplier' => Supplier::get()
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = FormData::find($id);
        $request->validate([
            'project_name' => 'required',
        ]);

        $data->type_data_id = Request()->type_data_id;
        $data->supplier_id = Request()->supplier_id;
        $data->item_name = Request()->item_name;
        $data->unit = Request()->unit;
        $data->quantity = Request()->quantity;
        $data->price = Request()->price;
        
        dd($data);
        // $data->save();
        // return redirect()->route('components.formulir.show', $data);
    }


    public function destroy(Form $id, $item_id)
    {
        FormData::destroy($item_id);
        return redirect()->route('formulir.show', $id);
    }
}
