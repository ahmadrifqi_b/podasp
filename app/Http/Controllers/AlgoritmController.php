<?php

namespace App\Http\Controllers;

use App\Models\{Form, FormData, TypeData, Supplier};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AlgoritmController extends Controller
{
    public function __construct()
    {
        $this->TypeData = new TypeData();
        $this->FormData = new FormData();
        $this->Form = new Form();
    }

    public function one()
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $type = $this->TypeData->get();

        if(request()->tanggal_transaksi) {

            $form_ids = $this->Form->whereBetween('date', explode(' - ', request()->all()['tanggal_transaksi']))->pluck('id')->toArray();
            $forms = $this->Form->whereBetween('date', explode(' - ', request()->all()['tanggal_transaksi']))->get();
            $form_datas = $this->FormData->whereIn('form_id', $form_ids)->get();
            $type = $type->whereIn('id', $form_datas->pluck('type_data_id')->toArray());

            return view ('algoritms', [
                'types' => $type,
                'forms' => $forms,
                'formDatas' => $form_datas,
            //    'data' => $data
            ]);
        }

        return view ('algoritms', [
            'types' => $type,
            'forms' => $this->Form->all(),
            'formDatas' => $this->FormData->all(),
        ]);

        // $forms = DB::table('forms')->get();
        // $data = DB::table('form_data')
        
        //         ->select(DB::raw('count(form_data.item_name) as total'), 'type_data.type_name')
        //         ->join('type_data', 'form_data.type_data_id', '=', 'type_data.id')
        //         //->join('forms', 'form_data.form_id', '=', 'forms.id')
                
        //         ->groupBy('type_data.type_name')
                
        //         //->orderBy('type_data.id', 'asc')
        //         ->get();
        // $type = DB::table('type_data')->get();
        // $intData = (int)$data;

        //$countData = DB::table('form_data')->count();
        // $intcount = (int)$countData;

        // $percent = 100;
        // $intpercent = (int)$percent;
        //echo $data;
        // foreach($data as $data){
        //     echo $data->total/$countData*100;
        // }


        // $forms = $this->Form->get();
        //$data = $this->FormData->get();
        // $countData = $this->FormData->count();
        //$type = $this->TypeData->get();

        //$data = FormData::with('category')->get();
        
        // $x = 0;
        // for($i = 1; $i <= 5; $i++){
        //     foreach ($data as $data) {
        //         if( isset($i) == isset($data->type_data_id)){
        //             ++$x;
        //         }
        //         echo $x;
        //     }
        // }
        
        

       
    }
}
