-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 26 Agu 2021 pada 17.30
-- Versi server: 10.4.18-MariaDB
-- Versi PHP: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `podasp`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `director_confirmeds`
--

CREATE TABLE `director_confirmeds` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `director_confirmeds`
--

INSERT INTO `director_confirmeds` (`id`, `name`) VALUES
(1, 'Menunggu Persetujuan'),
(2, 'Formulir Ditolak'),
(3, 'Formulir Diterima');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `finance_confirmeds`
--

CREATE TABLE `finance_confirmeds` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `finance_confirmeds`
--

INSERT INTO `finance_confirmeds` (`id`, `name`) VALUES
(1, 'Dana Belum Diturunkan'),
(2, 'Dana Sudah Diturunkan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `forms`
--

CREATE TABLE `forms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `po_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `project_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `telephone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `director_confirmed_id` bigint(20) DEFAULT NULL,
  `finance_confirmed_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `forms`
--

INSERT INTO `forms` (`id`, `user_id`, `po_number`, `project_name`, `address`, `delivery`, `email`, `date`, `telephone`, `fax`, `director_confirmed_id`, `finance_confirmed_id`, `created_at`, `updated_at`) VALUES
(1, 3, NULL, 'Kebayoran Village', 'Bintaro', NULL, NULL, '2019-01-01', NULL, NULL, 3, 2, '2021-05-03 08:31:31', '2021-05-05 10:21:17'),
(2, 3, NULL, 'Mess Boro', 'Angsana, Sungai Danau', 'Sungai Danau', NULL, '2020-01-02', NULL, NULL, 3, 2, '2021-05-04 10:38:23', '2021-05-07 20:03:07'),
(4, 3, NULL, 'CMS', NULL, NULL, NULL, '2019-01-02', NULL, NULL, 3, 2, '2021-05-05 19:11:41', '2021-05-09 07:08:15'),
(5, 3, '02/DASP', 'Pengadaan Jasa Untuk Jasa Site Restorasi 8 Lokasi Pemboran Sumur CBM Kab. Tabalong Bartim', 'Kab. Tabalong Bartim', 'Kab. Tabalong Bartim', NULL, '2021-05-06', '0', '0', 3, 2, '2021-05-05 19:19:38', '2021-05-05 19:19:38'),
(6, 3, NULL, 'Civil Maintenance Services', NULL, NULL, NULL, '2019-01-03', NULL, NULL, 3, 2, '2021-05-05 19:59:01', '2021-05-05 19:59:01'),
(7, 3, NULL, 'CMS', 'Jl. Raya Bojonegoro No. 40, Perum Mondokan Santoso, Merak Urak, Tuban', 'Bojonegoro', NULL, '2019-01-04', '0353881987', NULL, 3, 2, NULL, NULL),
(8, 3, NULL, 'RELOKASI GEDUNG KANTOR PEMERINTAH KE ZONA PERKANTORAN KALIBARU BARAT DAN PELABUHAN ANJUNG EMAS SEMARANG', 'Jl. Madukoro blok AA/ BB No. 3 Semarang', 'Semarang', NULL, '2019-01-04', '(024) 354 9098', NULL, 3, 1, NULL, '2021-06-22 04:15:31'),
(9, 3, NULL, 'Mixing Diva', 'Kp. Pesawahan RT 07/03 No 1 Depan Puskesmas Carita Labuan Pandeglang - Banten 42264', 'Pandeglang', NULL, '2019-01-05', '081910804776', NULL, 3, 2, NULL, NULL),
(10, 3, NULL, 'PEMBANGUNAN JALAN TOL BALIKPAPAN-SAMARINDA SEKSI 2,3 & 4', 'Jl. Letjend. Soeprapto RT. 28 No. 16, Baru Tengah, Balikpapan', 'Balikpapan', NULL, '2019-01-05', '0542 740462', NULL, 3, 2, NULL, NULL),
(11, 3, NULL, 'Mess Boro', 'Angsana, Sungai Danau', 'Sungai Danau', NULL, '2019-01-07', '0853-49740155', NULL, 3, 2, NULL, NULL),
(12, 3, NULL, 'CMS', '', 'Surabaya', NULL, '2019-01-07', '0822 2622 6810', NULL, 3, 2, NULL, NULL),
(13, 3, NULL, 'Mess Boro', 'Jln. Provinsi KM. 167 (Depan Damkar) Desa Sinar Bulan (Sungai Danau)', 'Desa Sinar Bulan', NULL, '2019-01-07', '0822-51548019', NULL, 3, 2, NULL, NULL),
(14, 3, NULL, 'PEMBANGUNAN MASJID DAARUL JANNAH KANTOR WALIKOTA JAKARTA SELATAN TAHAP II', 'Jl. Pluit Utara Raya no.611 Jakarta Barat', 'Jakarta Barat', NULL, '2019-01-09', '021-66675999', NULL, 3, 2, NULL, NULL),
(15, 3, NULL, 'CMS', 'Tangerang', 'Tangerang', NULL, '2019-01-10', NULL, NULL, 3, 2, NULL, NULL),
(16, 3, NULL, 'LANJUTAN RELOKASI GEDUNG KANTOR PEMERINTAH KE ZONA PERKANTORAN KALIBARU BARAT DAN PELABUHAN ANJUNG EMAS SEMARANG', 'Jl. Madukoro blok AA/ BB No. 3 Semarang', 'Semarang', NULL, '2019-01-11', '0857 5515 7005', NULL, 3, 2, NULL, NULL),
(17, 3, NULL, 'Mixing Diva', 'Pergudangan Mutiara Kosambi 1 Blok A3 no 2, Dadap, Tangerang', 'Tangerang', NULL, '2019-01-12', NULL, NULL, 3, 2, NULL, NULL),
(18, 3, NULL, 'Mess Boro', 'Jln. Provinsi KM. 167 (Depan Damkar) Desa Sinar Bulan (Sungai Danau)', 'Desa Sinar Bulan', NULL, '2019-01-12', '0822-51548019', NULL, 3, 2, NULL, NULL),
(19, 3, NULL, 'Mixing Mixa- Lhokseumawe', 'Jl. Pd. Betung Raya No. 8, Pd. Karya, Pd. Aren, Kota Tangerang Selatan, Banten 15221', '', NULL, '2019-01-15', '021-7340346', NULL, 3, 2, NULL, NULL),
(20, 3, NULL, 'LANJUTAN RELOKASI GEDUNG KANTOR PEMERINTAH KE ZONA PERKANTORAN KALIBARU BARAT DAN PELABUHAN ANJUNG EMAS SEMARANG', 'Jl. H. Agus Salim No. 10 Komplek Sentral Jurnatan', 'Semarang', NULL, '2019-01-15', '024-3549866', NULL, 3, 2, NULL, NULL),
(21, 3, NULL, 'CMS', 'Jl. KERTAJAYA NO. 132-134, SURABAYA', 'Surabaya', NULL, '2019-01-16', '0812 3245 7682', NULL, 3, 2, NULL, NULL),
(22, 3, NULL, 'PEMBANGUNAN MASJID DAARUL JANNAH KANTOR WALIKOTA JAKARTA SELATAN TAHAP II', 'Jl. Pluit Utara Raya no.611 Jakarta Barat', 'Jakarta Barat', NULL, '2019-01-17', '021-66675999', NULL, 3, 2, NULL, NULL),
(23, 3, NULL, 'LANJUTAN RELOKASI GEDUNG KANTOR PEMERINTAH KE ZONA PERKANTORAN KALIBARU BARAT DAN PELABUHAN ANJUNG EMAS SEMARANG', 'Jl. H. Agus Salim Ruko THD Baru Blok D-2 Semarang, 50137', 'Semarang', NULL, '2019-01-18', '(024) 354 9098', NULL, 3, 2, NULL, NULL),
(24, 3, NULL, 'PEMBANGUNAN MASJID DAARUL JANNAH KANTOR WALIKOTA JAKARTA SELATAN TAHAP II', 'Jl. Raya Parung Bogor No. 38 Parung Bogor 16330', 'Parung Bogor', NULL, '2019-01-21', '0251-8616688', NULL, 3, 2, NULL, NULL),
(25, 3, NULL, 'Mixing Mixa- Lhokseumawe', 'Jl. Cluster Eldora 2 Blok I1 no 25 Graha Raya Paku Jaya, Serpong Utara, Tangerang Selatan', 'Tangerang Selatan', NULL, '2019-01-19', '(021) 22927752', NULL, 3, 2, NULL, NULL),
(26, 3, NULL, 'PEMBANGUNAN MASJID DAARUL JANNAH KANTOR WALIKOTA JAKARTA SELATAN TAHAP II', 'Jl. Raya Parung Bogor No. 38 Parung Bogor 16330', 'Parung Bogor', NULL, '2019-01-21', '0251-8616688', NULL, 3, 2, NULL, NULL),
(27, 3, NULL, 'Mixing Mixa- Lhokseumawe', 'Jl. Pd. Betung Raya No. 8, Pd. Karya, Pd. Aren, Kota Tangerang Selatan, Banten 15221', 'Tangerang Selatan', NULL, '2019-01-22', '021-7340346', NULL, 3, 2, NULL, NULL),
(28, 3, NULL, 'Mess Boro', 'Jl. Soetoyo S No. 126 RT 24/ RW X Kel. Teluk Dalam Banjarmasin', 'Banjarmasin', NULL, '2019-01-22', '0822-5571-1113', NULL, 3, 2, NULL, NULL),
(29, 3, NULL, 'CMS', 'Komplek Harkot Blok E1/6&7 Jl. Merdeka Raya No. 53 Tangerang-Banten ', 'Tangerang Selatan', NULL, '2019-01-24', '021-55760519', NULL, 3, 2, NULL, NULL),
(30, 3, NULL, 'Mess Boro', 'Sungai Danau', 'Sungai Danau', NULL, '2019-01-25', '021-55760519', NULL, 3, 2, NULL, NULL),
(31, 3, NULL, 'CMS', 'Pondok Pakulonan H.8/20 RT 5 RW 4 Serpong Utara-Tangerang Selatan', 'Tangerang Selatan', NULL, '2019-01-25', '021 5399081', NULL, 3, 2, NULL, NULL),
(32, 3, NULL, 'Mess Boro', 'Jln. Propinsi m. 167 Sungai Danau Kec. Satui Kab. Tanah Bumbu', 'Tanah Bumbu', NULL, '2019-01-25', '0512-61200', NULL, 3, 2, NULL, NULL),
(33, 3, NULL, 'Pratama Residence', 'Jl. Daan Mogot No 48 C Jakarta Barat', 'Jakarta Barat', NULL, '2019-01-26', '(021) 5663130', NULL, 3, 2, NULL, NULL),
(34, 3, NULL, 'PEMBANGUNAN MASJID DAARUL JANNAH KANTOR WALIKOTA JAKARTA SELATAN TAHAP II', 'Jl. Raya Parung Bogor No. 38 Parung Bogor 16330', 'Parung Bogor', NULL, '2019-02-06', '0251-8616688', NULL, 3, 2, NULL, NULL),
(35, 3, NULL, 'Pratama Residence', 'Jl. Graha Raya Bintaro Fiera FR/D-19', 'Bintaro', NULL, '2019-01-30', '021 93513854', NULL, 3, 2, NULL, NULL),
(36, 3, NULL, 'CMS', 'Pondok Pakulonan H.8/20 RT 5 RW 4 Serpong Utara-Tangerang Selatan', 'Tangerang Selatan', NULL, '2019-01-30', '021 5399081', NULL, 3, 2, NULL, NULL),
(37, 3, NULL, 'LANJUTAN RELOKASI GEDUNG KANTOR PEMERINTAH KE ZONA PERKANTORAN KALIBARU BARAT DAN PELABUHAN TANJUNG EMAS', 'Jl. Citarum Raya No. 72 Semarang 50122', 'Semarang', NULL, '2019-02-02', '(024) 3545656', NULL, 3, 2, NULL, NULL),
(38, 3, NULL, 'Mess Boro', 'Jln. Provinsi KM. 167 (Depan Damkar) Desa Sinar Bulan (Sungai Danau)', 'Desa Sinar Bulan', NULL, '2019-02-02', '0852-11925-739', NULL, 3, 2, NULL, NULL),
(39, 3, NULL, 'CMS', 'Ds. Ngumpak Dalem, Kec. Dander, Bojonegoro', 'Ds. Ngumpak Dalem, Kec. Dander, Bojonegoro', NULL, '2019-02-05', '0813 3244 8562', NULL, 3, 2, NULL, NULL),
(40, 3, NULL, 'Civil Maintenance Services', 'Jl. Diponegoro, Bojonegoro', 'Bojonegoro', NULL, '2019-02-05', '0353 882329', NULL, 3, 2, NULL, NULL),
(41, 3, NULL, 'Mixing Mixa- Lhokseumawe', 'Jl. Pd. Betung Raya No. 88, Pd. Karya, Pd. Aren, Kota Tangerang Selatan, Banten 15221', 'Tangerang Selatan', NULL, '2019-02-06', '(021) 7340346', NULL, 3, 2, NULL, NULL),
(42, 3, NULL, 'CMS', 'Bojonegoro', 'Bojonegoro', NULL, '2019-02-07', NULL, NULL, 3, 2, NULL, NULL),
(43, 3, NULL, 'Lanjutan Relokasi Gedung Kantor Pemerintahan ', 'Semarang', 'Semarang', NULL, '2019-02-07', NULL, NULL, 3, 2, NULL, NULL),
(44, 3, NULL, 'Pengadaan Jasa untuk Jasa Site Restorasi 8', 'Kabupaten Tabalong Bartim', 'Kabupaten Tabalong Bartim', NULL, '2019-02-07', NULL, NULL, 3, 2, NULL, NULL),
(45, 3, NULL, 'Mess Boro', 'Sungai Danau', 'Sungai Danau', NULL, '2019-02-07', NULL, NULL, 3, 2, NULL, NULL),
(46, 3, NULL, 'CMS', 'Bojonegoro', 'Bojonegoro', NULL, '2019-02-09', NULL, NULL, 3, 2, NULL, NULL),
(47, 3, NULL, 'Mess Boro', 'Banjarmasin', 'Banjarmasin', NULL, '2019-02-09', NULL, NULL, 3, 2, NULL, NULL),
(48, 3, NULL, 'Mess Boro', 'Banjarmasin', 'Banjarmasin', NULL, '2019-02-11', NULL, NULL, 3, 2, NULL, NULL),
(49, 3, NULL, 'Mess Boro', 'Provinsi Angsana Tanah Bumbu', 'Provinsi Angsana Tanah Bumbu', NULL, '2019-02-13', NULL, NULL, 3, 2, NULL, NULL),
(50, 3, NULL, 'LANJUTAN RELOKASI GEDUNG KANTOR PEMERINTAHAN KE ZONA PERKANTORAN KALIBARU BARAT', 'Semarang', 'Semarang', NULL, '2019-01-14', NULL, NULL, 3, 2, NULL, NULL),
(51, 3, NULL, 'CMS', 'Bojonegoro', 'Bojonegoro', NULL, '2019-02-14', NULL, NULL, 3, 2, NULL, NULL),
(52, 3, NULL, 'Mess Boro', 'Sungai Danau', 'Sungai Danau', NULL, '2019-02-15', NULL, NULL, 3, 2, NULL, NULL),
(53, 3, NULL, 'Kebayoran Village', 'Graha Bintaro', 'Graha Bintaro', NULL, '2019-02-15', NULL, NULL, 3, 2, NULL, NULL),
(54, 3, NULL, 'CMS', 'Cepu', 'Cepu', NULL, '2019-02-16', NULL, NULL, 3, 2, NULL, NULL),
(55, 3, NULL, 'LANJUTAN RELOKASI GEDUNG KANTOR PEMERINTAH KE ZONA PERKANTORAN BARAT', 'Semarang', 'Semarang', NULL, '2019-02-18', NULL, NULL, 3, 2, NULL, NULL),
(56, 3, NULL, 'Ngumpak Dalem', 'Madiun', 'Madiun', NULL, '2019-02-19', NULL, NULL, 3, 2, NULL, NULL),
(57, 3, NULL, 'Mixing Mixa - Lhokseumawe', 'Tangerang ', 'Tangerang ', NULL, '2019-02-19', NULL, NULL, 3, 2, NULL, NULL),
(58, 3, NULL, 'Mess Boro', 'Sungai Danau', 'Sungai Danau', NULL, '2019-02-19', NULL, NULL, 3, 2, NULL, NULL),
(59, 3, NULL, 'CMS', 'Tangerang Selatan', 'Tangerang Selatan', NULL, '2019-02-20', NULL, NULL, 3, 2, NULL, NULL),
(60, 3, NULL, 'Civil Maintenace Services', 'Jakarta Barat', 'Jakarta Barat', NULL, '2019-02-25', NULL, NULL, 3, 2, NULL, NULL),
(61, 3, NULL, 'Mess Boro', 'Angsa Sungai Danau', 'Angsa Sungai Danau', NULL, '2019-02-27', NULL, NULL, 3, 2, NULL, NULL),
(62, 3, NULL, 'Civil Maintenace Services', 'Bojonegoro', 'Bojonegoro', NULL, '2019-02-28', NULL, NULL, 3, 2, NULL, NULL),
(63, 3, NULL, 'Civil Maintenace Service', 'Bojonegoro', 'Bojonegoro', NULL, '2019-03-01', NULL, NULL, 3, 2, NULL, NULL),
(64, 3, NULL, 'Mess Boro', 'Sungai Danau', 'Sungai Danau', NULL, '2019-03-02', NULL, NULL, 3, 2, NULL, NULL),
(65, 3, NULL, 'Civil Maintenace Service', 'Jakarta Barat', 'Jakarta Barat', NULL, '2019-03-03', NULL, NULL, 3, 2, NULL, NULL),
(66, 3, NULL, 'Civil Maintenace Service', 'Bojonegoro', 'Bojonegoro', NULL, '2019-03-05', NULL, NULL, 3, 2, NULL, NULL),
(67, 3, NULL, 'Mess Boro', 'Angsa Sungai Danau', 'Angsa Sungai Danau', NULL, '2019-03-08', NULL, NULL, 3, 2, NULL, NULL),
(68, 3, NULL, 'Civil Maintenace Service', 'Bojonegoro', 'Bojonegoro', NULL, '2019-03-09', NULL, NULL, 3, 2, NULL, NULL),
(69, 3, NULL, 'Civil Maintenace Service', 'Bojonegoro', 'Bojonegoro', NULL, '2019-03-11', NULL, NULL, 3, 2, NULL, NULL),
(70, 3, NULL, 'Mess Boro', 'Desa Sinar Bulan Sungai Danau', 'Desa Sinar Bulan Sungai Danau', NULL, '2019-03-12', NULL, NULL, 3, 2, NULL, NULL),
(71, 3, NULL, 'PEMBANGUNAN MASJID DAARUL JANNAH KANTOR WALIKOTA JAKARTA SELATAN TAHAP II', 'Jakarta Selatan', 'Jakarta Selatan', NULL, '2019-03-13', NULL, NULL, 3, 2, NULL, NULL),
(72, 3, NULL, 'Mess Boro', 'Desa Sinar Bulan Sungai Danau', 'Desa Sinar Bulan Sungai Danau', NULL, '2019-03-13', NULL, NULL, 3, 2, NULL, NULL),
(73, 3, NULL, 'Civil Maintenace Service', 'Surabaya', 'Surabaya', NULL, '2019-03-14', NULL, NULL, 3, 2, NULL, NULL),
(74, 3, NULL, 'PEMBANGUNAN MASJID DAARUL JANNAH KANTOR WALIKOTA JAKARTA SELATAN TAHAP II', 'Jakarta Selatan', 'Jakarta Selatan', NULL, '2019-03-15', NULL, NULL, 3, 2, NULL, NULL),
(75, 3, NULL, 'Civil Maintenace Service', NULL, NULL, NULL, '2019-03-15', NULL, NULL, 3, 2, NULL, NULL),
(76, 3, NULL, 'CMS', NULL, NULL, NULL, '2019-03-16', NULL, NULL, 3, 2, NULL, NULL),
(77, 3, NULL, 'Civil Maintenace Services', NULL, NULL, NULL, '2019-03-18', NULL, NULL, 3, 2, NULL, NULL),
(78, 3, NULL, 'Mess Boro', NULL, NULL, NULL, '2019-03-18', NULL, NULL, 3, 2, NULL, NULL),
(79, 3, NULL, 'CMS', NULL, NULL, NULL, '2019-03-21', NULL, NULL, 3, 2, NULL, NULL),
(80, 3, NULL, 'Ngumpak Dalem', NULL, NULL, NULL, '2019-03-21', NULL, NULL, 3, 2, NULL, NULL),
(81, 3, NULL, 'Kebayoran Village', NULL, NULL, NULL, '2019-03-21', NULL, NULL, 3, 2, NULL, NULL),
(82, 3, NULL, 'Mess Boro', NULL, NULL, NULL, '2019-03-21', NULL, NULL, 3, 2, NULL, NULL),
(83, 3, NULL, 'CMS', NULL, NULL, NULL, '2019-03-21', NULL, NULL, 3, 2, NULL, NULL),
(84, 3, NULL, 'Mess Boro', NULL, NULL, NULL, '2019-03-23', NULL, NULL, 3, 2, NULL, NULL),
(85, 3, NULL, 'CMS', NULL, NULL, NULL, '2019-03-23', NULL, NULL, 3, 2, NULL, NULL),
(86, 3, NULL, 'PEMBANGUNAN MASJID DAARUL JANNAH KANTOR WALIKOTA JAKARTA SELATAN', NULL, NULL, NULL, '2019-03-25', NULL, NULL, 3, 2, NULL, NULL),
(87, 3, NULL, 'CMS', NULL, NULL, NULL, '2019-03-25', NULL, NULL, 3, 2, NULL, NULL),
(88, 3, NULL, 'PEMBANGUNAN MASJID DAARUL JANNAH KANTOR WALIKOTA JAKARTA SELATAN', NULL, NULL, NULL, '2019-03-26', NULL, NULL, 3, 2, NULL, NULL),
(89, 3, NULL, 'Mess Boro', NULL, NULL, NULL, '2019-03-26', NULL, NULL, 3, 2, NULL, NULL),
(90, 3, NULL, 'CMS', NULL, NULL, NULL, '2019-03-26', NULL, NULL, 3, 2, NULL, NULL),
(91, 3, NULL, 'Mess Boro', NULL, NULL, NULL, '2019-03-27', NULL, NULL, 3, 2, NULL, NULL),
(92, 3, NULL, 'CMS', NULL, NULL, NULL, '2019-03-27', NULL, NULL, 3, 2, NULL, NULL),
(93, 3, NULL, 'CMS', NULL, NULL, NULL, '2019-03-28', NULL, NULL, 3, 2, NULL, NULL),
(94, 3, NULL, 'Mess Boro', NULL, NULL, NULL, '2019-03-29', NULL, NULL, 3, 2, NULL, NULL),
(95, 3, NULL, 'CMS', NULL, NULL, NULL, '2019-03-31', NULL, NULL, 3, 2, NULL, NULL),
(96, 3, NULL, 'Kebayoran Village', NULL, NULL, NULL, '2019-04-01', NULL, NULL, 3, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `form_data`
--

CREATE TABLE `form_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `form_id` bigint(20) UNSIGNED DEFAULT NULL,
  `type_data_id` bigint(20) UNSIGNED DEFAULT NULL,
  `supplier_id` bigint(20) UNSIGNED DEFAULT NULL,
  `item_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` bigint(20) DEFAULT NULL,
  `price` decimal(20,0) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `form_data`
--

INSERT INTO `form_data` (`id`, `form_id`, `type_data_id`, `supplier_id`, `item_name`, `unit`, `quantity`, `price`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 2, 'Batu Andesit', 'm2', 94, '250000', '2021-05-04 09:42:45', '2021-05-04 09:42:45'),
(2, 17, 29, NULL, 'KNO3', 'kg', 300, '25000', NULL, NULL),
(3, 18, 34, NULL, 'Batako UK 38 x 15 x 8', 'biji', 17080, '3000', NULL, NULL),
(4, 18, 35, NULL, 'Splite 1/2', 'm3', 35, '233000', NULL, NULL),
(5, 18, 9, NULL, 'Besi 10 Top', 'btg', 765, '84000', NULL, NULL),
(6, 18, 9, NULL, 'Besi 8 TOP', 'btg', 170, '51000', NULL, NULL),
(7, 18, 9, NULL, 'Besi 6 Top', 'btg', 516, '31000', NULL, NULL),
(8, 2, 4, 2, 'Batu Gunung Putih', 'm3', 94, '250000', '2021-05-05 10:09:58', '2021-05-05 10:09:58'),
(9, 18, 3, NULL, 'Kawat Bindrad', 'kg', 78, '18000', NULL, NULL),
(10, 4, 5, NULL, 'Anchor HLC 16x140x90', 'ea', 160, '25500', '2021-05-05 19:15:13', '2021-05-05 19:15:13'),
(11, 5, 4, NULL, 'Batu Gunung', 'm2', 200, '200000', '2021-05-05 19:21:14', '2021-05-05 19:21:14'),
(12, 4, 5, NULL, 'Anchor HLC 12x140x60', 'ea', 4, '14000', NULL, NULL),
(13, 4, 6, NULL, 'Zincalium 0,3 mm panjang 6 mtr, lebar 75 cm', 'mtr', 120, '40000', NULL, NULL),
(14, 6, 7, NULL, 'Gedheg 2x3 meter', 'lembar', 50, '75000', NULL, NULL),
(15, 7, 6, NULL, 'Listplank GRC Motif Lbr 30 cm', 'batang', 94, '42000', NULL, NULL),
(16, 7, 6, NULL, 'Listplank GRC Polos 20 cm', 'batang', 20, '27000', NULL, NULL),
(17, 7, 6, NULL, 'Wuwung Galvalum', 'lembar', 8, '61000', NULL, NULL),
(18, 7, 8, NULL, 'Keramik', 'm2', 64, '65000', NULL, NULL),
(19, 8, 9, NULL, 'Besi Hollow 50x100 Tebal 2.3 mm', 'batang', 20, '460000', NULL, NULL),
(20, 8, 10, NULL, 'Cat Dinding dalam Ecomulsion EE-4010 Broken White', 'pael', 14, '575000', NULL, NULL),
(21, 8, 10, NULL, 'Cat Plafon Eco Emulsion EE-4010, White', 'pael', 8, '575000', NULL, NULL),
(22, 8, 10, NULL, 'Sheler AR-300', 'pael', 7, '866150', NULL, NULL),
(23, 8, 10, NULL, 'Sheler utk dalam Ear-4001', 'pael', 6, '610300', NULL, NULL),
(24, 8, 11, NULL, 'Pasir', 'm3', 100, '265000', NULL, NULL),
(25, 8, 1, NULL, 'Semen', 'sak', 237, '41500', NULL, NULL),
(26, 8, 12, NULL, 'Silikon Warna Hitam Asam', 'dus', 10, '540000', NULL, NULL),
(27, 8, 13, NULL, 'Closet Duduk', 'unit', 10, '1630000', NULL, NULL),
(28, 8, 13, NULL, 'Urinoir', 'unit', 8, '2128000', NULL, NULL),
(29, 8, 14, NULL, 'Tandan Air isi 2000 ltr', 'unit', 2, '3200000', NULL, NULL),
(30, 8, 2, NULL, 'Batu Merah', 'm3', 10000, '525', NULL, NULL),
(31, 9, 1, NULL, 'Semen Gresik 40Kg', 'sak', 150, '41000', NULL, NULL),
(32, 9, 1, NULL, 'Semen Gresik 50Kg', 'sak', 160, '52000', NULL, NULL),
(33, 9, 1, NULL, 'Semen Gresik 50Kg', 'sak', 160, '52000', NULL, NULL),
(34, 10, 15, NULL, 'BBM Solar', 'liter', 50000, '10166', NULL, NULL),
(35, 11, 1, NULL, 'Semen Tiga Roda 50Kg', 'sak', 15, '62000', NULL, NULL),
(36, 11, 16, NULL, 'Kayu 2/20 144 batang', 'kubik', 2, '1400000', NULL, NULL),
(37, 11, 16, NULL, 'Kayu 5/10 45 batang', 'kubik', 1, '1400000', NULL, NULL),
(38, 11, 16, NULL, 'Kayu 5/7 93 batang', 'kubik', 1, '1400000', NULL, NULL),
(39, 11, 16, NULL, 'Kayu 3/5 80 batang', 'kubik', 1, '1400000', NULL, NULL),
(40, 12, 17, NULL, 'Baut M 18 Panjang 5 cm', 'pcs', 80, '11500', NULL, NULL),
(41, 12, 17, NULL, 'Baut M 18 Panjang 7 cm', 'pcs', 105, '12500', NULL, NULL),
(42, 12, 17, NULL, 'Baut M 10 Panjang 4 cm ', 'pcs', 76, '2350', NULL, NULL),
(43, 12, 17, NULL, 'Baut M 12 Panjang 4 cm', 'pcs', 100, '3200', NULL, NULL),
(44, 12, 18, NULL, 'Ring plat M10', 'pcs', 152, '1500', NULL, NULL),
(45, 12, 18, NULL, 'Ring plat M12', 'pcs', 200, '2000', NULL, NULL),
(46, 12, 18, NULL, 'Ring plat M18', 'pcs', 210, '2500', NULL, NULL),
(47, 13, 19, NULL, 'Spanduk', 'lembar', 27, '255000', NULL, NULL),
(48, 13, 20, NULL, 'Seng', 'lembar', 36, '48000', NULL, NULL),
(49, 13, 21, NULL, 'Paku 4\"', 'kg', 2, '17000', NULL, NULL),
(50, 13, 21, NULL, 'Paku 3\"', 'kg', 2, '17000', NULL, NULL),
(51, 13, 22, NULL, 'Karet Asbes', 'pcs', 20, '2000', NULL, NULL),
(52, 13, 23, NULL, 'Nusaboard', 'lembar', 107, '49000', NULL, NULL),
(53, 13, 24, NULL, 'Engsel', 'pcs', 12, '3000', NULL, NULL),
(54, 13, 25, NULL, 'Grendel', 'pcs', 6, '4000', NULL, NULL),
(55, 13, 25, NULL, 'Capingan nishio warna uk 2 1/2', 'pcs', 2, '5000', NULL, NULL),
(56, 13, 25, NULL, 'Capingan besi uk tanggung', 'bj', 2, '5000', NULL, NULL),
(57, 13, 25, NULL, 'Gembok oliq panjang uk 40', 'pcs', 1, '35000', NULL, NULL),
(58, 13, 25, NULL, 'Gembok oliq pendek uk 40', 'pcs', 2, '30000', NULL, NULL),
(59, 13, 26, NULL, 'Pipa AWS', 'batang', 15, '7000', NULL, NULL),
(60, 13, 21, NULL, 'Paku Kalsi', 'kg', 4, '23000', NULL, NULL),
(61, 13, 27, NULL, 'Lem Pipa', 'pcs', 1, '7000', NULL, NULL),
(62, 13, 1, NULL, 'Semen Conch 50kg', 'sak', 160, '52000', NULL, NULL),
(63, 13, 8, NULL, 'Keramik mutia 40x40 spec white', 'dus', 17, '58000', NULL, NULL),
(64, 13, 9, NULL, 'Besi Ø10 TOP', 'batang', 115, '84000', NULL, NULL),
(65, 13, 9, NULL, 'Besi Ø6 TOP', 'batang', 34, '34000', NULL, NULL),
(66, 13, 3, NULL, 'Kawat', 'kg', 10, '18000', NULL, NULL),
(67, 12, 3, NULL, 'Gabion Box', 'ea', 192, '259000', NULL, NULL),
(68, 12, 2, NULL, 'Enviroblast INDIA Garnet 30/60', 'kg', 2000, '8500', NULL, NULL),
(69, 13, 4, NULL, 'Semen Tiga Roda 50kg', 'sak', 200, '62000', NULL, NULL),
(70, 14, 9, NULL, 'Besi Ulir 10mm', 'kg', 2960, '8300', NULL, NULL),
(71, 14, 9, NULL, 'Besi Ulir 13mm', 'kg', 2496, '8300', NULL, NULL),
(72, 14, 9, NULL, 'Besi Ulir 16mm', 'kg', 4740, '8300', NULL, NULL),
(73, 14, 9, NULL, 'Besi Ulir 19mm', 'kg', 1338, '8350', NULL, NULL),
(74, 14, 30, NULL, 'Ongkos Kirim PO Nomor 028', NULL, 1, '1800000', NULL, NULL),
(75, 15, 9, NULL, 'Besi 10 SNI 5000 btng', 'kg', 37000, '8250', NULL, NULL),
(76, 15, 9, NULL, 'Besi 8 SNI 500 BTNG Polos', 'kg', 2400, '8250', NULL, NULL),
(77, 15, 9, NULL, 'Besi 25 SNI 150 BTG Ulir', 'kg', 6930, '8450', NULL, NULL),
(78, 15, 30, NULL, 'Steel Plate 2mm SNI', 'lbr', 25, '470000', NULL, NULL),
(79, 15, 9, NULL, 'L 40 x 40 x 2mm - 6 meter', 'btg', 175, '93000', NULL, NULL),
(80, 15, 9, NULL, 'Wiremesh M8 - 150', 'lbr', 400, '590000', NULL, NULL),
(81, 16, 10, NULL, 'Cat Dinding Luar Ecoshiel ES-600 Broken White', 'pael', 5, '799000', NULL, NULL),
(82, 16, 26, NULL, 'Pipa PVC 4\" AW', 'btg', 2, '215000', NULL, NULL),
(83, 16, 26, NULL, 'Pipa PVC 3\" AW', 'btg', 22, '129000', NULL, NULL),
(84, 16, 32, NULL, 'Keni PVC 4\"', 'unit', 2, '9000', NULL, NULL),
(85, 16, 32, NULL, 'Keni PVC 3\"', 'unit', 18, '6500', NULL, NULL),
(86, 17, 33, NULL, 'Jumbo Bag 1 Ton', 'pcs', 300, '40000', NULL, NULL),
(87, 17, 29, NULL, 'Pottasium Clorida Kcl', 'kg', 300, '9000', NULL, NULL),
(88, 17, 29, NULL, 'Magnesium Clorida Mcl', 'kg', 360, '8000', NULL, NULL),
(89, 17, 29, NULL, 'Sodium Clorida', 'kg', 340, '6000', NULL, NULL),
(90, 17, 29, NULL, 'Kalsium Clorida', 'kg', 700, '5500', NULL, NULL),
(91, 18, 1, NULL, 'Semen Tiga Roda 50 Kg', 'sak', 200, '49000', NULL, NULL),
(92, 19, 1, NULL, 'Semen Gresik 50 Kg', 'sak', 160, '51000', NULL, NULL),
(93, 20, 36, NULL, 'Lampu Taman TO. 10 TL. E 27, Opal Glass Plastik Mounting Komplit', 'bh', 13, '397000', NULL, NULL),
(94, 20, 3, NULL, 'Kawat BC 35mm', 'mtr', 30, '44000', NULL, NULL),
(95, 20, 37, NULL, 'Plat Acrilic 1.22 x 2.44', 'lbr', 6, '1320000', NULL, NULL),
(96, 20, 30, NULL, 'Hollow Diameter 40x40x10 T 1mm', 'btg', 16, '300000', NULL, NULL),
(97, 20, 18, NULL, 'Ring Diameter 40 x 40', 'pcs', 30, '6500', NULL, NULL),
(98, 20, 28, NULL, 'Kaca Kanopi Tempered 10mm', 'unit', 30, '950000', NULL, NULL),
(99, 20, 38, NULL, 'Polish 1000 x 2000', 'm2', 180, '15000', NULL, NULL),
(100, 20, 38, NULL, 'Polish 600 x 3000', 'm2', 15, '15000', NULL, NULL),
(101, 20, 28, NULL, 'kaca Jendela Tempered 10 mm', 'unit', 2, '855000', NULL, NULL),
(102, 21, 36, NULL, '600mm 8W 765 T8 AP C G', 'ea', 16, '40000', NULL, NULL),
(103, 21, 39, NULL, 'Battery Emergency LED LAMP 20 W', 'gln', 9, '939250', NULL, NULL),
(104, 21, 29, NULL, 'Thinner no 17', 'gln', 5, '262500', NULL, NULL),
(105, 22, 9, NULL, 'Besi Ulir 13mm', 'kg', 2494, '8300', NULL, NULL),
(106, 22, 9, NULL, 'Besi Ulir 16mm', 'kg', 1896, '8300', NULL, NULL),
(107, 22, 9, NULL, 'Besi Ulir 19mm', 'kg', 8028, '8350', NULL, NULL),
(108, 22, 31, NULL, 'Ongkos Kirim PO', 'satuan', 1, '1800000', NULL, NULL),
(109, 18, 11, NULL, 'Pasir Hitam Pelatihan', 'm3', 81, '288000', NULL, NULL),
(110, 21, 30, NULL, 'Plat 2 mm ', 'kg', 1175, '10850', NULL, NULL),
(111, 21, 9, NULL, 'L 40 x 40 x 4mm x 6 mtr', 'kg', 2526, '10200', NULL, NULL),
(112, 21, 9, NULL, 'Besi Beton Ulir D16 mm x 12 mtr SNI HKHK', 'kg', 4664, '8150', NULL, NULL),
(113, 24, 16, NULL, 'Kayu Kaso', 'm3', 5, '2500000', NULL, NULL),
(114, 24, 16, NULL, 'kayu Balok 6/12 MC', 'm3', 5, '2600000', NULL, NULL),
(115, 24, 40, NULL, 'Multiplek 12mm', 'lbr', 50, '134000', NULL, NULL),
(116, 24, 40, NULL, 'Multiplek 9mm', 'lbr', 75, '93000', NULL, NULL),
(117, 25, 41, NULL, 'Filter Solar', 'pc', 1, '375000', NULL, NULL),
(118, 25, 41, NULL, 'Filter Oli Mesin', 'pc', 1, '375000', NULL, NULL),
(119, 25, 1, NULL, 'Semen Gresik 50kg', 'sak', 160, '51000', NULL, NULL),
(120, 25, 39, NULL, 'Aki Nagoya', 'pc', 1, '1450000', NULL, NULL),
(121, 26, 42, NULL, 'Beton ReadyMix Mutu K-300', 'm3', 150, '720000', NULL, NULL),
(122, 26, 16, NULL, 'Kayu Kaso', 'm3', 10, '2500000', NULL, NULL),
(123, 26, 16, NULL, 'Kayu Balok', 'm3', 10, '2600000', NULL, NULL),
(124, 26, 40, NULL, 'Multiplek 12mm', 'lbr', 50, '134000', NULL, NULL),
(125, 27, 1, NULL, 'Semen Gresik 50 kg', 'sak', 160, '52000', NULL, NULL),
(126, 27, 1, NULL, 'Semen Gresik 50 kg', 'sak', 160, '52000', NULL, NULL),
(127, 27, 29, NULL, 'MAPEY Mapeplast Silicafume', 'kg', 5000, '6900', NULL, NULL),
(128, 28, 1, NULL, 'Semen Tiga Roda 50 kg', 'sak', 605, '49000', NULL, NULL),
(129, 28, 26, NULL, 'Pipa Wavin 4\" AW', 'btg', 20, '295000', NULL, NULL),
(130, 29, 43, NULL, 'Bearing Pillow Blok', 'ls', 6, '65000', NULL, NULL),
(131, 29, 43, NULL, 'Pully Seling 4\"', 'ls', 7, '135000', NULL, NULL),
(132, 29, 43, NULL, 'Pully B1 4\"', 'ls', 1, '43000', NULL, NULL),
(133, 29, 44, NULL, 'Generator Genset Matahari 7000', 'ls', 1, '8750000', NULL, NULL),
(134, 29, 44, NULL, 'Dinamo 1HP', 'ls', 1, '1250000', NULL, NULL),
(135, 30, 16, NULL, 'Kayu 2/20', 'm3', 4, '1400000', NULL, NULL),
(136, 30, 31, NULL, 'Ongkos Kirim PO 114', 'unit', 2, '250000', NULL, NULL),
(137, 31, 30, NULL, 'Plat 6mm', 'lbr', 140, '10100', NULL, NULL),
(138, 31, 30, NULL, 'Plat 2mm', 'lbr', 25, '471000', NULL, NULL),
(139, 31, 9, NULL, 'Hollow 75x75x3mm', 'btg', 5, '498000', NULL, NULL),
(140, 31, 9, NULL, 'Hollow 50x50x3mm', 'btg', 33, '320000', NULL, NULL),
(141, 31, 9, NULL, 'L 40x40x4mm', 'kg', 2523, '9650', NULL, NULL),
(142, 31, 9, NULL, 'Besi Beton Ulir 16mmx12mtr', 'kg', 4665, '8300', NULL, NULL),
(143, 31, 9, NULL, 'Wiremesh', 'kg', 3090, '10400', NULL, NULL),
(144, 31, 31, NULL, 'Ongkos Kirim PO 119', 'unit', 1, '1200000', NULL, NULL),
(145, 32, 26, NULL, 'Pipa Wavin 4\" AW', 'btg', 46, '310000', NULL, NULL),
(146, 32, 26, NULL, 'Pipa Wavin 3\" AW', 'btg', 60, '205000', NULL, NULL),
(147, 32, 32, NULL, 'LBOW 4\"', 'bh', 29, '125000', NULL, NULL),
(148, 32, 32, NULL, 'SOCK 4\"', 'bh', 25, '65000', NULL, NULL),
(149, 32, 32, NULL, 'KNEE 3\"', 'bh', 50, '55000', NULL, NULL),
(150, 32, 32, NULL, 'TEE 3\"', 'bh', 40, '65000', NULL, NULL),
(151, 32, 32, NULL, 'SOCK 3\"', 'bh', 10, '45000', NULL, NULL),
(152, 32, 32, NULL, 'T Y 4\"', 'bh', 6, '155000', NULL, NULL),
(153, 33, 45, NULL, 'Flat Excelence Hitam Pekat', 'pcs', 1650, '6000', NULL, NULL),
(154, 33, 45, NULL, 'Nok UG Spc Flat Hitam Pekat', '3', 27500, NULL, NULL, NULL),
(155, 33, 45, NULL, 'Nok UG Flat Accent Hitam pekat', 'pcs', 46, '7500', NULL, NULL),
(156, 33, 34, NULL, 'Hebel', 'm3', 13, '660000', NULL, NULL),
(157, 34, 9, NULL, 'Besi Ulir 13mm', 'kg', 2496, '8400', NULL, NULL),
(158, 34, 9, NULL, 'Besi Ulir 16mm', 'kg', 11850, '8400', NULL, NULL),
(159, 34, 31, NULL, 'Ongkos Kirim PO 181', 'unit', 1, '1800000', NULL, NULL),
(160, 35, 1, NULL, 'Mortar DM 100', 'sak', 10, '85000', NULL, NULL),
(161, 35, 9, NULL, 'Hollow 4 x 2', 'btg', 45, '20000', NULL, NULL),
(162, 35, 9, NULL, 'Hollow 4 x 4 ', 'btg', 10, '25000', NULL, NULL),
(163, 35, 21, NULL, 'Paku Beton 5cm', 'dus', 3, '50000', NULL, NULL),
(164, 35, 23, NULL, 'Gypsum yosino', 'lbr', 15, '60000', NULL, NULL),
(165, 35, 1, NULL, 'Compound a plus', 'sak', 65000, NULL, NULL, NULL),
(166, 35, 17, NULL, 'Sekrup Gypsum 2cm', 'dus', 3, '85000', NULL, NULL),
(167, 36, 9, NULL, 'Wiremesh M8 - 150', 'kg', 3090, '10400', NULL, NULL),
(168, 36, 9, NULL, 'Besi Beton Ulir 13mm x 12mtr', 'kg', 313, '8300', NULL, NULL),
(169, 36, 9, NULL, 'Besi beton 8mm x 12mtr', 'kg', 948, '8200', NULL, NULL),
(170, 37, 12, NULL, 'Silikon Warna Hitam Asam', 'dus', 10, '540000', NULL, NULL),
(171, 37, 9, NULL, 'Besi Hollow 40 x 40 2,3mm', 'btg', 20, '460000', NULL, NULL),
(172, 38, 20, NULL, 'Seng Bekas', 'lbr', 95, '24000', NULL, NULL),
(173, 38, 11, NULL, 'Pasir halus', 'rit', 1, '700000', NULL, NULL),
(174, 38, 35, NULL, 'Sirtu ', 'rit', 5, '700000', NULL, NULL),
(175, 38, 35, NULL, 'Splite', 'rit', 1, '1400000', NULL, NULL),
(176, 38, 9, NULL, 'besi 12 TOP', 'btg', 85, '125000', NULL, NULL),
(177, 38, 9, NULL, 'Besi 8 TOP', 'btg', 88, '54000', NULL, NULL),
(178, 38, 3, NULL, 'Kawat', 'kg', 30, '18000', NULL, NULL),
(179, 39, 1, NULL, 'Semen Gresik', 'sak', 50, '50000', NULL, NULL),
(180, 39, 11, NULL, 'Pasir', 'm3', 7, '200000', NULL, NULL),
(181, 39, 35, NULL, 'Splite', 'm3', 7, '250000', NULL, NULL),
(182, 39, 40, NULL, 'Melamin', 'ea', 4, '105000', NULL, NULL),
(183, 39, 37, NULL, 'Akrilik Clear Blue', 'lbr', 7, '910000', NULL, NULL),
(184, 39, 37, NULL, 'Akrilik Brown', 'lbr', 1, '910000', NULL, NULL),
(185, 39, 16, NULL, 'Papan 2 mtr x 40 cm x 2 cm', 'lbr', 2, '270000', NULL, NULL),
(186, 39, 16, NULL, 'Papan 3 mtr x 40 cm x 2 cm', 'lbr', 6, '350000', NULL, NULL),
(187, 39, 17, NULL, 'STD Anchor HSA M8', 'ea', 205, '5100', NULL, NULL),
(188, 39, 45, NULL, 'Listplank 20', 'lbr', 7, '42000', NULL, NULL),
(189, 39, 16, NULL, 'Kaso 4 x 6', 'btg', 1, '70000', NULL, NULL),
(190, 39, 40, NULL, 'Triplek 8mm', 'lbr', 1, '97000', NULL, NULL),
(191, 39, 21, NULL, 'Paku bekesting', 'kg', 1, '14000', NULL, NULL),
(192, 39, 47, NULL, 'BC Cable 70mm', 'mtr', 10, '71000', NULL, NULL),
(193, 39, 48, NULL, 'Scoen Cable BC 70', 'ea', 10, '17000', NULL, NULL),
(194, 39, 48, NULL, 'Scoen Cable BC 25', 'ea', 30, '13000', NULL, NULL),
(195, 40, 26, NULL, 'Pipa Condult Clipsal 1 1/4', 'btg', 17, '11500', NULL, NULL),
(196, 40, 17, NULL, 'Clamp Condult', 'eq', 50, '2000', NULL, NULL),
(197, 39, 42, NULL, 'Concrete Cylinder mold', 'pcs', 15, '400000', NULL, NULL),
(198, 41, 1, NULL, 'Semen Gresik 50 KG', 'sak', 160, '51000', NULL, NULL),
(199, 37, 9, NULL, 'Besi Hollow 40 x 40 ', 'btg', 20, '460000', NULL, NULL),
(200, 37, 28, NULL, 'Kaca Bening 5mm', 'm2', 127, '100000', NULL, NULL),
(201, 37, 27, NULL, 'Sealent', 'dus', 1, '600000', NULL, NULL),
(202, 37, 28, NULL, 'Kaca Es 5mm', 'm2', 15, '110000', NULL, NULL),
(203, 42, 34, NULL, 'Hebel', 'm3', 2, '680000', NULL, NULL),
(204, 42, 24, NULL, 'Siku Aluminium Elastis', 'ea', 33, '45000', NULL, NULL),
(205, 42, 10, NULL, 'Jotun Gardex premium Ral 7001', 'ltr', 10, '68240', NULL, NULL),
(206, 42, 10, NULL, 'Jotun Gardex premium Ral 1017', 'ltr', 10, '83580', NULL, NULL),
(207, 42, 10, NULL, 'Cat Catylac Outdoor', 'pil', 1, '875000', NULL, NULL),
(208, 43, 24, NULL, 'Tiang Engsel', 'ea', 5, '309900', NULL, NULL),
(209, 43, 28, NULL, 'Casment', 'ea', 8, '387000', NULL, NULL),
(210, 43, 10, NULL, 'Cat Dinding Dalam Ecomulsion EE-4010', 'pael', 2, '575000', NULL, NULL),
(211, 43, 10, NULL, 'Cat Dinding Luar Ecoshiel ES 600', 'pael', 3, '799000', NULL, NULL),
(212, 43, 8, NULL, 'Keramik Venus 60 x 60', 'dm2', 14, '222100', NULL, NULL),
(213, 44, 15, NULL, 'Solar Industri', 'ltr', 2000, '13745', NULL, NULL),
(214, 45, 16, NULL, 'kayu 2/20', 'm3', 2, '1400000', NULL, NULL),
(215, 45, 16, NULL, 'kayu 5/10', 'm3', 1, '1400000', NULL, NULL),
(216, 45, 16, NULL, 'Kayu 5/7', 'm3', 1, '1400000', NULL, NULL),
(217, 45, 16, NULL, 'Kayu 4/6', 'm3', 1, '1400000', NULL, NULL),
(218, 45, 16, NULL, 'Kayu 3/5', 'm3', 1, '1400000', NULL, NULL),
(219, 45, 16, NULL, 'Kayu 5/5', 'm3', 1, '1400000', NULL, NULL),
(220, 45, 31, NULL, 'Ongkos Kirim PO 184', 'unit', 2, '250000', NULL, NULL),
(221, 46, 1, NULL, 'Semen Holcim', 'sak', 200, '42500', NULL, NULL),
(222, 46, 11, NULL, 'Pasir Cor', 'm3', 3, '250000', NULL, NULL),
(223, 46, 35, NULL, 'Kerikil Koral 1-2', 'm3', 2, '280000', NULL, NULL),
(224, 46, 1, NULL, 'Semen Holcim', 'sak', 10, '42500', NULL, NULL),
(225, 47, 6, NULL, 'Baja RIngan TASO Roofmart C 75 75', 'btg', 490, '78637', NULL, NULL),
(226, 47, 6, NULL, 'Baja Ringan TASO Roofmart Reng 32 45', 'btg', 680, '38182', NULL, NULL),
(227, 47, 31, NULL, 'Ongkos Kirim PO 191', 'unit', 1, '1800000', NULL, NULL),
(228, 48, 31, NULL, 'Ongkos Kirim PO 198', 'unit', 1, '1800000', NULL, NULL),
(229, 48, 6, NULL, 'Besi Ringan TASO Roofmart C 75 75', 'btg', 296, '78637', NULL, NULL),
(230, 48, 6, NULL, 'Besi Ringan TASO Roofmart Reng 32 45', 'btg', 360, '38182', NULL, NULL),
(231, 49, 9, NULL, 'Besi 10 TOP', 'btg ', 200, '77000', NULL, NULL),
(232, 49, 26, NULL, 'Pipa PVC 21/2 Aw', 'btg', 20, '125000', NULL, NULL),
(233, 49, 32, NULL, 'Knee 21/2', 'btg', 40, '10000', NULL, NULL),
(234, 50, 12, NULL, 'Silikon Warna Hitam Asam 286', 'dus', 10, '540000', NULL, NULL),
(235, 51, 45, NULL, 'Talang Galvalum 90', 'mtr', 5, '45000', NULL, NULL),
(236, 51, 40, NULL, 'Triplek 8mm', 'lbr', 4, '97000', NULL, NULL),
(237, 51, 29, NULL, 'Thinner A Special', 'ltr', 10, '40000', NULL, NULL),
(238, 51, 21, NULL, 'Paku 7cm', 'kg', 1, '20000', NULL, NULL),
(239, 51, 27, NULL, 'Lem VIP', 'kg', 6, '15000', NULL, NULL),
(240, 51, 3, NULL, 'Kawat Scaffolding', 'roll', 4, '35000', NULL, NULL),
(241, 51, 16, NULL, 'Kaso 4 x 6 x 300', 'btg', 100, '16000', NULL, NULL),
(242, 51, 29, NULL, 'Eco Diesel', 'tab', 80, '25000', NULL, NULL),
(243, 51, 45, NULL, 'Atap Galvalum 0,3 mm 80cm', 'mtr', 160, '38000', NULL, NULL),
(244, 52, 16, NULL, 'kayu 2/20', 'kubik', 4, '1400000', NULL, NULL),
(245, 52, 16, NULL, 'kayu 5/10', 'kubik', 1, '1400000', NULL, NULL),
(246, 52, 31, NULL, 'Ongkos Kirim PO 205', 'unit', 2, '250000', NULL, NULL),
(247, 53, 1, NULL, 'Mortar MU 420', 'sak', 10, '85000', NULL, NULL),
(248, 54, 11, NULL, 'Abu Batu 3 rit', 'm3', 21, '200000', NULL, NULL),
(249, 54, 26, NULL, 'GSP SCH40 2\"', 'kg', 2056, '18000', NULL, NULL),
(250, 55, 28, NULL, 'Kaca Bening 5mm', 'm2', 23, '100000', NULL, NULL),
(251, 55, 28, NULL, 'Kaca bening 10mm', 'm2', 6, '345000', NULL, NULL),
(252, 55, 28, NULL, 'Slep bening 10mm', 'm2 ', 31, '12000', NULL, NULL),
(253, 56, 15, NULL, 'Solar', 'liter', 5000, '9100', NULL, NULL),
(254, 57, 1, NULL, 'Semen Gresik 50kg', 'sak', 160, '51000', NULL, NULL),
(255, 57, 1, NULL, 'Semen Gresik 50kg', 'sak', 160, '51000', NULL, NULL),
(256, 58, 16, NULL, 'Kayu 2/20', 'm3', 1, '1400000', NULL, NULL),
(257, 58, 16, NULL, 'Kayu 5/10', 'm3', 1, '1400000', NULL, NULL),
(258, 58, 16, NULL, 'Kayu 4/6', 'm3', 1, '1400000', NULL, NULL),
(259, 58, 16, NULL, 'Kayu 2/20', 'm3', 2, '1400000', NULL, NULL),
(260, 58, 16, NULL, 'Kayu 5/10', 'm3', 1, '1400000', NULL, NULL),
(261, 58, 16, NULL, 'Kayu 5/7', 'm3', 1, '1400000', NULL, NULL),
(262, 58, 16, NULL, 'Kayu 4/6', 'm3', 1, '1400000', NULL, NULL),
(263, 58, 31, NULL, 'Ongkos Kirim PO 225', 'unit', 1, '250000', NULL, NULL),
(264, 59, 30, NULL, 'Steel Plate 22mm', 'kg', 12312, '9800', NULL, NULL),
(265, 59, 30, NULL, 'Steel Plate 10mm', 'kg', 233, '9800', NULL, NULL),
(266, 59, 30, NULL, 'Steel Plate 8mm', 'kg', 374, '9800', NULL, NULL),
(267, 59, 9, NULL, 'besi Beton Ulir D10mm 12m U40 SNI', 'kg', 3700, '8400', NULL, NULL),
(268, 59, 9, NULL, 'Besi Beton Ulir D13mm 12m U40 SNIO', 'kg', 1875, '8400', NULL, NULL),
(269, 60, 28, NULL, 'Convex Mirror D120cm', 'ea', 1, '2618000', NULL, NULL),
(270, 60, 35, NULL, 'pedel', 'm3', 2000, '75000', NULL, NULL),
(271, 60, 17, NULL, 'STD Anchor HAS M12x100', 'ea', 450, '13000', NULL, NULL),
(272, 60, 9, NULL, 'CNP 75', 'btg', 12, '170000', NULL, NULL),
(273, 60, 9, NULL, 'UNP 100', 'btg', 62, '490000', NULL, NULL),
(274, 60, 9, NULL, 'UNP 150', 'btg', 86, '1090000', NULL, NULL),
(275, 60, 9, NULL, 'Siku 40', 'btg', 12, '110000', NULL, NULL),
(276, 60, 9, NULL, 'Siku 50', 'btg', 55, '200000', NULL, NULL),
(277, 60, 9, NULL, 'Siku 70', 'btg', 24, '430000', NULL, NULL),
(278, 61, 35, NULL, 'Splite 1/2', 'rit', 14, '1500000', NULL, NULL),
(279, 61, 11, NULL, 'Pasir Hitam Pelaihan', 'rit', 20, '1300000', NULL, NULL),
(280, 61, 1, NULL, 'Semen Tiga Roda 50kg', 'sak', 400, '49000', NULL, NULL),
(281, 61, 9, NULL, 'Wiremesh M8', 'lbr', 54, '830000', NULL, NULL),
(282, 62, 10, NULL, 'jotun Gardex RAL 7001', 'ltr', 60, '68000', NULL, NULL),
(283, 62, 10, NULL, 'Emco White', 'ltr', 15, '60000', NULL, NULL),
(284, 62, 10, NULL, 'Emco Yellow Ral 1004', 'ltr', 3, '78000', NULL, NULL),
(285, 62, 10, NULL, 'Emco Hitam', 'ltr', 3, '60000', NULL, NULL),
(286, 62, 29, NULL, 'Thinner', 'galon', 6, '135000', NULL, NULL),
(287, 62, 19, NULL, 'Cetak Form \"Daily Delivery material\"', 'rim', 1, '250000', NULL, NULL),
(288, 62, 19, NULL, 'Cetak \"FPB\"', 'rim', 1, '250000', NULL, NULL),
(289, 62, 19, NULL, 'Cetak \"FPBS\"', 'rim', 1, '250000', NULL, NULL),
(290, 62, 19, NULL, 'Cetak \"Site Daly Report\"', 'rim', 1, '250000', NULL, NULL),
(291, 62, 19, NULL, 'Cetak \"Mecanic Daily Report\"', 'rim', 1, '250000', NULL, NULL),
(292, 62, 19, NULL, 'Cetak \"Jurney Management\"', 'rim', 1, '250000', NULL, NULL),
(293, 62, 11, NULL, 'Pasir Cor', 'm3', 21, '205000', NULL, NULL),
(294, 62, 11, NULL, 'Pasir Pasang', 'm3', 21, '205000', NULL, NULL),
(295, 62, 35, NULL, 'Splite 1/2', 'm3', 7, '280000', NULL, NULL),
(296, 63, 9, NULL, 'Steel Grating WA 254', 'lbr', 14, '4280000', NULL, NULL),
(297, 63, 11, NULL, 'Abu Batu 3 rit', 'm3', 21, '200000', NULL, NULL),
(298, 63, 9, NULL, 'Hollow 75.75.3,5mm', 'kg', 880, '11145', NULL, NULL),
(299, 63, 9, NULL, 'Hollow 50.50.3mm', 'kg', 84, '12000', NULL, NULL),
(300, 63, 9, NULL, 'Hollow 30.30.3mm', 'kg', 420, '11000', NULL, NULL),
(301, 63, 9, NULL, 'Pipa besi 2\"', 'kg', 65, '18000', NULL, NULL),
(302, 63, 9, NULL, 'Baja C75.75', 'btg', 110, '77500', NULL, NULL),
(303, 63, 45, NULL, 'Ridge Caping ', 'lbr', 4, '61000', NULL, NULL),
(304, 63, 6, NULL, 'Reng Baja RIngan', 'btg', 110, '77500', NULL, NULL),
(305, 63, 6, NULL, 'Hollow Baja Ringan 40.40.4mm', 'btg', 45, '20000', NULL, NULL),
(306, 63, 6, NULL, 'Hollow Baja Rngan 40.20.2mm', 'btg', 47, '14000', NULL, NULL),
(307, 64, 26, NULL, 'Pipa 20mm', 'btg', 30, '13000', NULL, NULL),
(308, 64, 32, NULL, 'T DOS 20mm', 'pcs', 40, '5000', NULL, NULL),
(309, 64, 32, NULL, 'LB Dos Kotak', 'pcs', 30, '5000', NULL, NULL),
(310, 64, 47, NULL, 'NYM 2 x 2,5', 'roll', 1, '950000', NULL, NULL),
(311, 64, 47, NULL, 'NYM 3 x 2,5', 'roll', 1, '1150000', NULL, NULL),
(312, 64, 27, NULL, 'isolasi', 'bh', 2, '5000', NULL, NULL),
(313, 64, 31, NULL, 'Pemakaian Mobil Period 6 maret - 5 april 2019', 'unit', 1, '8000000', NULL, NULL),
(314, 65, 9, NULL, 'Steel Pipe 1 1/4\" x 6m SCH', 'btg', 200, '271000', NULL, NULL),
(315, 65, 9, NULL, 'Steel Pipe 1 1/2\" x 6m SCH', 'btg', 30, '322000', NULL, NULL),
(316, 65, 9, NULL, 'Steel Pipe 2 1/2\" x 6m SCH', 'btg', 120, '685000', NULL, NULL),
(317, 65, 32, NULL, 'PCV T Dos CL', 'ea', 20, '6000', NULL, NULL),
(318, 65, 47, NULL, 'NYY 3 x 2,5 Eterna', 'roll', 2, '1500000', NULL, NULL),
(319, 65, 9, NULL, 'Besi Beton Ulir D10mm x 12mtr', 'kg', 7400, '8400', NULL, NULL),
(320, 65, 9, NULL, 'Besi Beton Ulir D13mm x 12mtr', 'kg', 500, '8400', NULL, NULL),
(321, 65, 9, NULL, 'Besi Beton Polos P32 mm x 12 mtr', 'kg', 3787, '8400', NULL, NULL),
(322, 66, 30, NULL, 'Plat Besi 0,7mm', 'lbr', 10, '240000', NULL, NULL),
(323, 66, 49, NULL, 'Coverall Biasa', 'ea', 20, '135000', NULL, NULL),
(324, 66, 49, NULL, 'Sepatu Boot', 'psg', 20, '165000', NULL, NULL),
(325, 66, 49, NULL, 'Kacamata CIG', 'psg', 24, '35000', NULL, NULL),
(326, 66, 49, NULL, 'Helm Putih', 'box', 20, '37500', NULL, NULL),
(327, 67, 35, NULL, 'Batu Gunung Putih', 'rit', 6, '1250000', NULL, NULL),
(328, 67, 9, NULL, 'Hollow 4x4 Merk A Plus', 'btg', 1000, '21000', NULL, NULL),
(329, 67, 9, NULL, 'Hollow 2x4 Merk A Plus', 'btg', 150, '17000', NULL, NULL),
(330, 68, 1, NULL, 'Semen Holcim ', 'sak', 200, '42500', NULL, NULL),
(331, 68, 31, NULL, 'Sewa Mobil Tangki Air 5000 Ltr', 'unit', 2, '8500000', NULL, NULL),
(332, 68, 49, NULL, 'Sepatu Crusher Uk 38', 'psg', 3, '400000', NULL, NULL),
(333, 68, 49, NULL, 'Sepatu Crusher Uk 39', 'psg', 1, '400000', NULL, NULL),
(334, 68, 49, NULL, 'Sepatu Crusher Uk 40', 'psg', 2, '400000', NULL, NULL),
(335, 68, 49, NULL, 'Sepatu Crusher Uk 41', 'psg', 2, '400000', NULL, NULL),
(336, 68, 49, NULL, 'Sepatu Crusher Uk 42', 'psg', 2, '400000', NULL, NULL),
(337, 68, 49, NULL, 'Ear Plug', 'ea', 50, '4000', NULL, NULL),
(338, 69, 9, NULL, 'Steel Plate 6mm ', 'lbr', 1, '1498000', NULL, NULL),
(339, 69, 44, NULL, 'Mesin Bor', 'ea', 1, '740000', NULL, NULL),
(340, 69, 23, NULL, 'Gypsum Board STAR 9mm', 'lbr', 3, '56000', NULL, NULL),
(341, 69, 10, NULL, 'No Drop Clear', 'ltr', 3, '195000', NULL, NULL),
(342, 69, 3, NULL, 'Kawat Anten', 'roll', 3, '40000', NULL, NULL),
(343, 69, 49, NULL, 'Kacamata Kin Hitam', 'ea', 24, '25000', NULL, NULL),
(344, 69, 35, NULL, 'Splite 1/2', 'm3', 1, '280000', NULL, NULL),
(345, 69, 11, NULL, 'Pasir Cor Gre', 'm3', 4, '250000', NULL, NULL),
(346, 70, 1, NULL, 'Semen Tiga Roda 50kg', 'sak', 80, '56000', NULL, NULL),
(347, 70, 9, NULL, 'Besi P10 SNI', 'btg', 210, '84000', NULL, NULL),
(348, 70, 31, NULL, 'Ongkos Kirim ', 'unit', 1, '100000', NULL, NULL),
(349, 70, 3, NULL, 'Kawat Bendrat', 'kg', 5, '18000', NULL, NULL),
(350, 70, 6, NULL, 'Baja Ringan TASO C 75.75', 'btg', 70, '86000', NULL, NULL),
(351, 70, 11, NULL, 'Pasir Hitam Pelaihan', 'rit', 13, '1300000', NULL, NULL),
(352, 71, 26, NULL, 'Pipa PVC AW 4\"', 'btg', 14, '275000', NULL, NULL),
(353, 71, 26, NULL, 'Pipa PVC AW 2\"', 'btg', 9, '80000', NULL, NULL),
(354, 72, 23, NULL, 'Gypsum Board 9mm', 'lbr', 250, '60000', NULL, NULL),
(355, 72, 1, NULL, 'Kalsi', 'lbr', 110, '49000', NULL, NULL),
(356, 73, 45, NULL, 'Atap Galvalum 0,3mm', 'mtr', 300, '41000', NULL, NULL),
(357, 73, 40, NULL, 'Bitumen Impregenated Fiberboard', 'lbr', 4, '484000', NULL, NULL),
(358, 73, 30, NULL, '', NULL, NULL, NULL, NULL, NULL),
(359, 73, 9, NULL, '', NULL, NULL, NULL, NULL, NULL),
(360, 73, 35, NULL, '', NULL, NULL, NULL, NULL, NULL),
(361, 73, 10, NULL, '', NULL, NULL, NULL, NULL, NULL),
(362, 73, 29, NULL, '', NULL, NULL, NULL, NULL, NULL),
(363, 74, 32, NULL, '', NULL, NULL, NULL, NULL, NULL),
(364, 74, 32, NULL, '', NULL, NULL, NULL, NULL, NULL),
(365, 75, 9, NULL, '', NULL, NULL, NULL, NULL, NULL),
(366, 75, 9, NULL, '', NULL, NULL, NULL, NULL, NULL),
(367, 75, 9, NULL, '', NULL, NULL, NULL, NULL, NULL),
(368, 75, 31, NULL, '', NULL, NULL, NULL, NULL, NULL),
(369, 75, 1, NULL, '', NULL, NULL, NULL, NULL, NULL),
(370, 75, 26, NULL, '', NULL, NULL, NULL, NULL, NULL),
(371, 75, 49, NULL, '', NULL, NULL, NULL, NULL, NULL),
(372, 75, 49, NULL, '', NULL, NULL, NULL, NULL, NULL),
(373, 75, 26, NULL, '', NULL, NULL, NULL, NULL, NULL),
(374, 75, 30, NULL, '', NULL, NULL, NULL, NULL, NULL),
(375, 76, 30, NULL, '', NULL, NULL, NULL, NULL, NULL),
(376, 76, 9, NULL, '', NULL, NULL, NULL, NULL, NULL),
(377, 76, 9, NULL, '', NULL, NULL, NULL, NULL, NULL),
(378, 76, 9, NULL, '', NULL, NULL, NULL, NULL, NULL),
(379, 76, 9, NULL, '', NULL, NULL, NULL, NULL, NULL),
(380, 77, 47, NULL, '', NULL, NULL, NULL, NULL, NULL),
(381, 77, 47, NULL, '', NULL, NULL, NULL, NULL, NULL),
(382, 77, 47, NULL, '', NULL, NULL, NULL, NULL, NULL),
(383, 77, 47, NULL, '', NULL, NULL, NULL, NULL, NULL),
(384, 78, 16, NULL, '', NULL, NULL, NULL, NULL, NULL),
(385, 78, 27, NULL, '', NULL, NULL, NULL, NULL, NULL),
(386, 78, 21, NULL, '', NULL, NULL, NULL, NULL, NULL),
(387, 78, 24, NULL, '', NULL, NULL, NULL, NULL, NULL),
(388, 78, 44, NULL, '', NULL, NULL, NULL, NULL, NULL),
(389, 78, 43, NULL, '', NULL, NULL, NULL, NULL, NULL),
(390, 78, 3, NULL, '', NULL, NULL, NULL, NULL, NULL),
(391, 78, 26, NULL, '', NULL, NULL, NULL, NULL, NULL),
(392, 78, 32, NULL, '', NULL, NULL, NULL, NULL, NULL),
(393, 78, 26, NULL, '', NULL, NULL, NULL, NULL, NULL),
(394, 78, 1, NULL, '', NULL, NULL, NULL, NULL, NULL),
(395, 78, 31, NULL, '', NULL, NULL, NULL, NULL, NULL),
(396, 79, 16, NULL, '', NULL, NULL, NULL, NULL, NULL),
(397, 79, 47, NULL, '', NULL, NULL, NULL, NULL, NULL),
(398, 80, 15, NULL, '', NULL, NULL, NULL, NULL, NULL),
(399, 81, 26, NULL, '', NULL, NULL, NULL, NULL, NULL),
(400, 82, 12, NULL, '', NULL, NULL, NULL, NULL, NULL),
(401, 82, 17, NULL, '', NULL, NULL, NULL, NULL, NULL),
(402, 82, 21, NULL, '', NULL, NULL, NULL, NULL, NULL),
(403, 83, 41, NULL, '', NULL, NULL, NULL, NULL, NULL),
(404, 83, 41, NULL, '', NULL, NULL, NULL, NULL, NULL),
(405, 83, 31, NULL, '', NULL, NULL, NULL, NULL, NULL),
(406, 83, 41, NULL, '', NULL, NULL, NULL, NULL, NULL),
(407, 83, 41, NULL, '', NULL, NULL, NULL, NULL, NULL),
(408, 83, 31, NULL, '', NULL, NULL, NULL, NULL, NULL),
(409, 83, 9, NULL, '', NULL, NULL, NULL, NULL, NULL),
(410, 83, 9, NULL, '', NULL, NULL, NULL, NULL, NULL),
(411, 83, 30, NULL, '', NULL, NULL, NULL, NULL, NULL),
(412, 83, 30, NULL, '', NULL, NULL, NULL, NULL, NULL),
(413, 84, 31, NULL, '', NULL, NULL, NULL, NULL, NULL),
(414, 84, 31, NULL, '', NULL, NULL, NULL, NULL, NULL),
(415, 84, 42, NULL, '', NULL, NULL, NULL, NULL, NULL),
(416, 84, 10, NULL, '', NULL, NULL, NULL, NULL, NULL),
(417, 85, 35, NULL, '', NULL, NULL, NULL, NULL, NULL),
(418, 85, 40, NULL, '', NULL, NULL, NULL, NULL, NULL),
(419, 85, 21, NULL, '', NULL, NULL, NULL, NULL, NULL),
(420, 85, 21, NULL, '', NULL, NULL, NULL, NULL, NULL),
(421, 85, 16, NULL, '', NULL, NULL, NULL, NULL, NULL),
(422, 85, 26, NULL, '', NULL, NULL, NULL, NULL, NULL),
(423, 85, 32, NULL, '', NULL, NULL, NULL, NULL, NULL),
(424, 85, 27, NULL, '', NULL, NULL, NULL, NULL, NULL),
(425, 85, 26, NULL, '', NULL, NULL, NULL, NULL, NULL),
(426, 85, 40, NULL, '', NULL, NULL, NULL, NULL, NULL),
(427, 85, 21, NULL, '', NULL, NULL, NULL, NULL, NULL),
(428, 85, 23, NULL, '', NULL, NULL, NULL, NULL, NULL),
(429, 85, 10, NULL, '', NULL, NULL, NULL, NULL, NULL),
(430, 85, 10, NULL, '', NULL, NULL, NULL, NULL, NULL),
(431, 85, 45, NULL, '', NULL, NULL, NULL, NULL, NULL),
(432, 85, 8, NULL, '', NULL, NULL, NULL, NULL, NULL),
(433, 85, 44, NULL, '', NULL, NULL, NULL, NULL, NULL),
(434, 85, 26, NULL, '', NULL, NULL, NULL, NULL, NULL),
(435, 86, 32, NULL, '', NULL, NULL, NULL, NULL, NULL),
(436, 87, 49, NULL, '', NULL, NULL, NULL, NULL, NULL),
(437, 87, 49, NULL, '', NULL, NULL, NULL, NULL, NULL),
(438, 87, 49, NULL, '', NULL, NULL, NULL, NULL, NULL),
(439, 87, 49, NULL, '', NULL, NULL, NULL, NULL, NULL),
(440, 87, 17, NULL, '', NULL, NULL, NULL, NULL, NULL),
(441, 87, 18, NULL, '', NULL, NULL, NULL, NULL, NULL),
(442, 87, 47, NULL, '', NULL, NULL, NULL, NULL, NULL),
(443, 88, 9, NULL, '', NULL, NULL, NULL, NULL, NULL),
(444, 88, 9, NULL, '', NULL, NULL, NULL, NULL, NULL),
(445, 89, 34, NULL, '', NULL, NULL, NULL, NULL, NULL),
(446, 89, 9, NULL, '', NULL, NULL, NULL, NULL, NULL),
(447, 89, 9, NULL, '', NULL, NULL, NULL, NULL, NULL),
(448, 90, 44, NULL, '', NULL, NULL, NULL, NULL, NULL),
(449, 90, 44, NULL, '', NULL, NULL, NULL, NULL, NULL),
(450, 91, 45, NULL, '', NULL, NULL, NULL, NULL, NULL),
(451, 91, 31, NULL, '', NULL, NULL, NULL, NULL, NULL),
(452, 91, 8, NULL, '', NULL, NULL, NULL, NULL, NULL),
(453, 92, 31, NULL, '', NULL, NULL, NULL, NULL, NULL),
(454, 92, 49, NULL, '', NULL, NULL, NULL, NULL, NULL),
(455, 93, 31, NULL, '', NULL, NULL, NULL, NULL, NULL),
(456, 93, 44, NULL, '', NULL, NULL, NULL, NULL, NULL),
(457, 93, 44, NULL, '', NULL, NULL, NULL, NULL, NULL),
(458, 93, 17, NULL, '', NULL, NULL, NULL, NULL, NULL),
(459, 93, 17, NULL, '', NULL, NULL, NULL, NULL, NULL),
(460, 94, 16, NULL, '', NULL, NULL, NULL, NULL, NULL),
(461, 94, 16, NULL, '', NULL, NULL, NULL, NULL, NULL),
(462, 95, 49, NULL, '', NULL, NULL, NULL, NULL, NULL),
(463, 95, 49, NULL, '', NULL, NULL, NULL, NULL, NULL),
(464, 95, 9, NULL, '', NULL, NULL, NULL, NULL, NULL),
(465, 95, 9, NULL, '', NULL, NULL, NULL, NULL, NULL),
(466, 95, 9, NULL, '', NULL, NULL, NULL, NULL, NULL),
(467, 95, 9, NULL, '', NULL, NULL, NULL, NULL, NULL),
(468, 95, 31, NULL, '', NULL, NULL, NULL, NULL, NULL),
(469, 96, 35, NULL, '', NULL, NULL, NULL, NULL, NULL),
(470, 96, 35, NULL, '', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2014_10_12_200000_add_two_factor_columns_to_users_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(6, '2021_04_04_075044_create_sessions_table', 1),
(7, '2021_04_04_081122_create_roles_table', 1),
(8, '2021_04_20_104654_create_forms_table', 2),
(9, '2021_04_20_104713_create_form_data_table', 2),
(10, '2021_04_20_104814_create_type_data_table', 2),
(11, '2021_04_20_104848_create_suppliers_table', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`role_id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator Super User', NULL, NULL),
(2, 'director', 'Direktur Perusahaan', NULL, NULL),
(3, 'purchaser', 'Bagian Purchasing', NULL, NULL),
(4, 'finance', 'Departemen Keuangan', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('5MpwLNXVUNiaVIxJqwv3V05RHnqmUaFht6zrjZ2i', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.78', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoiZ2NtQTdXSkNpR21QQ3ZUc1VBdDJXQ0kyM3U0TU1vOHpTd0NwT1hZQSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzE6Imh0dHA6Ly8xMjcuMC4wLjE6ODAwMC9kYXNoYm9hcmQiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aToxO3M6MTc6InBhc3N3b3JkX2hhc2hfd2ViIjtzOjYwOiIkMnkkMTAkeHFwVXBZVy56eUVJT0FsUUdPeG9tTzM0SkE1Vy9lRG1mcnBxaC9kd2Zmb3dLclJvZUR0ZzIiO3M6MjE6InBhc3N3b3JkX2hhc2hfc2FuY3R1bSI7czo2MDoiJDJ5JDEwJHhxcFVwWVcuenlFSU9BbFFHT3hvbU8zNEpBNVcvZURtZnJwcWgvZHdmZm93S3JSb2VEdGcyIjt9', 1629991732);

-- --------------------------------------------------------

--
-- Struktur dari tabel `suppliers`
--

CREATE TABLE `suppliers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `supplier_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `suppliers`
--

INSERT INTO `suppliers` (`id`, `supplier_name`, `address`, `telephone`, `created_at`, `updated_at`) VALUES
(1, 'Varia Logam\r\n', 'Jl. Rajawali No.68, Bojonegoro\r\n', '353881987', NULL, NULL),
(2, 'RADJASTONE', 'Pergudangan Kavling DPR, Blok A / 212, Jl. Hasyim Ashari, depan Polsek Cipondoh, Tangerang.', '816911926', '2021-04-28 05:59:26', '2021-04-28 05:59:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `type_data`
--

CREATE TABLE `type_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `type_data`
--

INSERT INTO `type_data` (`id`, `type_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Semen', 'Semen Bangunan Tiga Roda', '2021-04-28 22:55:57', '2021-04-28 23:25:22'),
(2, 'Batu Bata', 'Batu Bata Pondasi Bangunan', '2021-04-28 23:25:46', '2021-04-28 23:25:46'),
(3, 'Kawat', 'Kawat Tipis', '2021-04-28 23:25:59', '2021-04-28 23:25:59'),
(4, 'Batu', 'Bebatuan Gunung', '2021-05-04 09:31:35', '2021-05-04 09:31:35'),
(5, 'HLC', 'HLC', '2021-05-05 19:14:46', '2021-05-05 19:14:46'),
(6, 'Baja Ringan', 'Baja Ringan', NULL, NULL),
(7, 'Gedheg', 'Gedheg', NULL, NULL),
(8, 'Keramik', 'Keramik', NULL, NULL),
(9, 'Besi', 'Besi', NULL, NULL),
(10, 'Cat', 'Cat', NULL, NULL),
(11, 'Pasir', 'Pasir', NULL, NULL),
(12, 'Silikon', 'Silikon', NULL, NULL),
(13, 'Perlengkapan Toilet', 'Perlengkapan Toilet', NULL, NULL),
(14, 'Tandon Air', 'Tandon Air', NULL, NULL),
(15, 'Bahan Bakar', 'Bahan Bakar', NULL, NULL),
(16, 'Kayu', 'Kayu', NULL, NULL),
(17, 'Baut', 'baut', NULL, NULL),
(18, 'Ring Baut', 'Ring Plat Baut', NULL, NULL),
(19, 'Spanduk', 'Spanduk', NULL, NULL),
(20, 'Seng', 'Seng', NULL, NULL),
(21, 'Paku', 'Paku', NULL, NULL),
(22, 'Karet Asbes', 'Karet Asbes', NULL, NULL),
(23, 'Gypsum', 'Gypsum', NULL, NULL),
(24, 'Engsel', 'Engsel', NULL, NULL),
(25, 'Kunci / Gembok', 'Pengaman ', NULL, NULL),
(26, 'Pipa', 'Pipa', NULL, NULL),
(27, 'Lem', 'Perekat', NULL, NULL),
(28, 'Kaca', 'Kaca', NULL, NULL),
(29, 'Bahan Kimia', 'Bahan Kimia', NULL, NULL),
(30, 'Plat Besi', 'Plat Besi', NULL, NULL),
(31, 'Bayar Jasa', 'Jasa Perorangan, Tukang, dan Pengiriman.', NULL, NULL),
(32, 'Keni Pipa', 'Keni Pipa', NULL, NULL),
(33, 'Jumbo Bag', 'Jumbo Bag', NULL, NULL),
(34, 'Batako', 'batu Batako', NULL, NULL),
(35, 'Batu Kerikil', 'batu splite / kerikil', NULL, NULL),
(36, 'Lampu ', 'Lampu', NULL, NULL),
(37, 'Akrilik', 'Kaca Akrilik', NULL, NULL),
(38, 'Polish', 'Polish', NULL, NULL),
(39, 'Baterai', 'Baterai', NULL, NULL),
(40, 'Triplek / Multiplek', 'Kayu Triplek atau Kayu Multiplek', NULL, NULL),
(41, 'Sparepart Kendaraan', 'Perlengkapan Suku Cadang Kendaraan', NULL, NULL),
(42, 'Beton', 'Beton', NULL, NULL),
(43, 'Bearing', 'Bearing', NULL, NULL),
(44, 'Mesin', 'Mesin / Alat Berat', NULL, NULL),
(45, 'Genteng', 'Genteng', NULL, NULL),
(46, 'Compound', 'Compound', NULL, NULL),
(47, 'Kabel', 'Kabel', NULL, NULL),
(48, 'Skun Kabel', 'Skun Kabel / Scun Cable', NULL, NULL),
(49, 'Alat Pelindung Keselamatan', 'Alat Pelindung Keselamatan', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) UNSIGNED DEFAULT NULL,
  `profile_photo_path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `current_team_id`, `profile_photo_path`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, 'admin@admin.com', '2021-04-04 01:37:33', '$2y$10$xqpUpYW.zyEIOAlQGOxomO34JA5W/eDmfrpqh/dwffowKrRoeDtg2', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(2, 'pengguna', NULL, 'ahmad.rifqi@outlook.co.id', NULL, '$2y$10$TlX4hTw4/CyHZRi9HB9gW.hKtpo03l4HSEw4spkX0LxRtkc8IcJ.i', NULL, NULL, NULL, NULL, NULL, 1, '2021-04-04 01:58:24', '2021-04-04 01:58:24'),
(3, 'direktur', NULL, 'direktur@email.com', NULL, '$2y$10$T00YOch58auN5LjYjK3n2Ou0B2TG54PbWS7jqTJdQFyy2QofpU2NG', NULL, NULL, NULL, NULL, NULL, 2, '2021-05-02 17:48:42', '2021-05-02 17:48:42'),
(5, 'purchasing', NULL, 'purchasing@email.com', NULL, '$2y$10$csdtuJGg/Ne5bOTiRrOASeba4r.SmgtrTvSjclenY2fsGw5YulwIq', NULL, NULL, NULL, NULL, NULL, 3, '2021-05-02 17:49:51', '2021-05-02 17:49:51'),
(6, 'bendahara', NULL, 'bendahara@email.com', NULL, '$2y$10$p1D.pt2t6Zqhsyq5.Uxuq.XGcXJUHV0F9v6/c1Er76fyMpC22TFaW', NULL, NULL, NULL, NULL, NULL, 4, '2021-05-02 17:50:06', '2021-05-02 17:50:06');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `director_confirmeds`
--
ALTER TABLE `director_confirmeds`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `finance_confirmeds`
--
ALTER TABLE `finance_confirmeds`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `form_data`
--
ALTER TABLE `form_data`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indeks untuk tabel `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- Indeks untuk tabel `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `type_data`
--
ALTER TABLE `type_data`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `director_confirmeds`
--
ALTER TABLE `director_confirmeds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `finance_confirmeds`
--
ALTER TABLE `finance_confirmeds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `forms`
--
ALTER TABLE `forms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT untuk tabel `form_data`
--
ALTER TABLE `form_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=471;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `type_data`
--
ALTER TABLE `type_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
