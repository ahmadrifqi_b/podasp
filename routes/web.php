<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\FormController;
use App\Http\Controllers\FormDataController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\TypeDataController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AlgoritmController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    return view('auth.login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    //Route::get('/formulir', [FormController::class, 'index'])->name('formulir');
    //Route::get('/formulir/create', [FormController::class, 'create']);

    // Route::get('/jenis-barang', function() {
    //     return view('type-item');
    // })->name('kategori');

    Route::get('/algorithms', [AlgoritmController::class, 'one'])->name('apriori');

    //Tab Purchase Order
    Route::prefix('formulir')->name('formulir.')->group(function () {
        Route::get('/', [FormController::class, 'index'])->name('index');
        Route::get('/create', [FormController::class, 'create'])->name('create');
        Route::get('/show/{id}', [FormController::class, 'show'])->name('show');
        Route::post('/store/', [FormController::class, 'store'])->name('store');
        Route::get('/edit/{id}', [FormController::class, 'edit'])->name('edit');
        Route::put('/update/{id}', [FormController::class, 'update'])->name('update');

        Route::get('/approval/{id}', [FormController::class, 'editdirector'])->name('approval');
        Route::put('/approvalacc/{id}', [FormController::class, 'updatedirector'])->name('approvalacc');
        Route::get('/funds/{id}', [FormController::class, 'editfincance'])->name('funds');
        Route::put('/fundsacc/{id}', [FormController::class, 'updatefinance'])->name('fundsacc');

        Route::get('/delete/{id}', [FormController::class, 'destroy'])->name('delete');
        //Item Barang Relasi Purchase order
        Route::prefix('{id}/item')->name('item.')->group(function ($id) {
            Route::get('/', [FormDataController::class, 'index'])->name('index');
            Route::get('/create', [FormDataController::class, 'create'])->name('create');
            Route::post('/store', [FormDataController::class, 'store'])->name('store');
            //Route::get('/show/{item_id}', [FormDataController::class, 'show'])->name('show');
            //Route::put('/update/{item_id}', [FormDataController::class, 'update'])->name('update');
            Route::get('/delete/{item_id}', [FormDataController::class, 'destroy'])->name('delete');
        });
    });

    //Kategori barang
    Route::prefix('kategori')->name('kategori.')->group(function () {
        Route::get('/', [TypeDataController::class, 'index'])->name('index');
        Route::get('/create', [TypeDataController::class, 'create'])->name('create');
        Route::get('/show/{id}', [TypeDataController::class, 'show'])->name('show');
        Route::post('/store/', [TypeDataController::class, 'store'])->name('store');
        Route::put('/update/{id}', [TypeDataController::class, 'update'])->name('update');
        Route::get('/delete/{id}', [TypeDataController::class, 'destroy'])->name('delete');
    });

    //Tab Supplier
    Route::prefix('pemasok')->name('pemasok.')->group(function () {
        Route::get('/', [SupplierController::class, 'index'])->name('index');
        Route::get('/create', [SupplierController::class, 'create'])->name('create');
        Route::get('/show/{id}', [SupplierController::class, 'show'])->name('show');
        Route::post('/store/', [SupplierController::class, 'store'])->name('store');
        Route::put('/update/{id}', [SupplierController::class, 'update'])->name('update');
        Route::get('/delete/{id}', [SupplierController::class, 'destroy'])->name('delete');
    });
    
    //Tab Karyawan
    Route::prefix('karyawan')->name('karyawan.')->group(function () {
        Route::get('/', [UserController::class, 'index'])->name('index');
        Route::get('/create', [UserController::class, 'create'])->name('create');
        Route::get('/store', [UserController::class, 'store'])->name('store');
        //Route::get('/delete/{id}', [UserController::class, 'destroy'])->name('delete');
    });
});