<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->string('po_number', 100)->nullable();
            $table->string('project_name');
            $table->string('address')->nullable();
            $table->string('delivery')->nullable();
            $table->string('email', 100)->nullable();
            $table->date('date');
            $table->smallInteger('telephone')->nullable();
            $table->smallInteger('fax')->nullable();
            $table->boolean('confirmed')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms');
    }
}
